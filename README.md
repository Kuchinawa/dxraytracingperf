Research project gathering performance figures for DirectX Raytracing. 
 
The program will automatically detect if a suitable driver for DXR is available.
If not, it will run through the fallback compute path.
 
Because DXR is a new experimental feature Developer mode has to be turned on before running the application.
(Settings -> For developers -> Tick developer mode)

The program will loop through 18 settings variations and 6 shaders, 2 of these shaders take only a few frames to run, but the other 4 take about 10 seconds.
The complete run on the application takes about 12 minutes.

This project uses code from the (c) Microsoft DirectX-Graphics-Samples repository, which are licensed under the MIT License (MIT).

NOTE: As of November 2018 this application will only run on windows 10 v.1803, there is wip in a branch to lift this to the current version.