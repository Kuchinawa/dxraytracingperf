#include "precomp.h"
#include "SystemContext.h"

bool closing = false;

SystemContext::SystemContext(UINT width, UINT height, std::wstring windowName) :
	windowBounds{ 0, 0, 0, 0 },
	windowTitle(windowName)
{
}


SystemContext::~SystemContext()
{
}

//Change the title text next to the window title
void SystemContext::SetTitle(LPCWSTR text)
{
	std::wstring newTitle = windowTitle + L": " + text;
	SetWindowText(hwnd, newTitle.c_str());
}

int SystemContext::Run(HINSTANCE hInstance, int nCmdShow, WNDPROC wndProc)
{
	try
	{
		//Parse CMD
		int argc;
		LPWSTR* argv = CommandLineToArgvW(GetCommandLineW(), &argc);
		ParseCommandLineArgs(argv, argc);
		LocalFree(argv); //Free the memory block

		//Initialize the window
		WNDCLASSEX windowClass = { 0 };
		windowClass.cbSize = sizeof(WNDCLASSEX);
		windowClass.style = CS_HREDRAW | CS_VREDRAW;
		windowClass.lpfnWndProc = wndProc;
		windowClass.hInstance = hInstance;
		windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		windowClass.lpszClassName = L"DXRPerfClass";
		RegisterClassEx(&windowClass);

		RECT windowRect = { 0, 0, static_cast<LONG>(RESWIDTH), static_cast<LONG>(RESHEIGHT) };
		AdjustWindowRect(&windowRect, windowStyle, FALSE);

		//Create the window and store the handle
		hwnd = CreateWindow(
			windowClass.lpszClassName,
			windowTitle.c_str(),
			windowStyle,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			windowRect.right - windowRect.left,
			windowRect.bottom - windowRect.top,
			nullptr, //Parent window
			nullptr, //Menu
			hInstance,
			nullptr //variable sent to the message handler (lparam)
		);

		//Show window
		ShowWindow(hwnd, nCmdShow);


		float testDurationInSec = 10.0f;

		const UINT modelCount = 3;

		//Load (OBJ) model files
		std::string modelNames[modelCount] =
		{
			"Stanford Dragon 50k.obj",
			"Stanford Dragon 100k.obj",
			"Stanford Dragon 200k.obj"
		};

		std::vector<std::shared_ptr<Model>> models;
		models.reserve(3);
		for (UINT i = 0; i < modelCount; i++)
		{
			models.push_back(std::make_shared<Model>("assets/" + modelNames[i]));
		}


		//Loop through force AS rebuild off/on
		for (UINT rebuild = 0; rebuild < 2; rebuild++)
		{
			//Loop through AS build types Fast trace, and Fast build
			for (UINT buildType = 0; buildType < 2; buildType++)
			{
				//Loop through the models
				for (UINT i = 0; i < modelCount; i++)
				{
					RunTest(models[i], testDurationInSec, modelNames[i], ASBuildType(buildType), rebuild, false);

					if (closing)
					{
						//Stop other tests from executing
						return 0;
					}
				}
			}
		}

		//Finally test AS again with allow updates
		//Loop through AS build types Fast trace, and Fast build
		for (UINT buildType = 0; buildType < 2; buildType++)
		{
			//Loop through the models
			for (UINT i = 0; i < modelCount; i++)
			{
				RunTest(models[i], testDurationInSec, modelNames[i], ASBuildType(buildType), true, true);

				if (closing)
				{
					//Stop other tests from executing
					return 0;
				}
			}
		}

	return 0;
	//return static_cast<char>(msg.wParam);
}
catch (const std::exception& ex)
{
	OutputDebugString(L"Exception: ");
	OutputDebugStringA(ex.what());
	OutputDebugString(L"\nClosing...\n");

	//printf(ex.what());
	
	PerfWriter::Instance().Write(ex.what());

	//Cleanup
	fallbackContext->OnDestroy();

	return EXIT_FAILURE;
}
}


void SystemContext::RunTest(std::shared_ptr<Model> model, float testDurationInSec, std::string modelName, ASBuildType buildTypeAS, bool rebuildAS, bool updateAS)
{
	std::stringstream logString;

	logString << "-------- Model " << modelName << "--------\n";

	switch (buildTypeAS)
	{
	case FastTrace:
		logString << "-------- AS build type: Fast Trace / ";
		break;
	case FastBuild:
		logString << "-------- AS build type: Fast Build / ";
		break;
	}

	switch (rebuildAS)
	{
	case false:
		logString << "Rebuild AS OFF --------\n";
		break;
	case true:
		logString << "Rebuild AS ON /";

		switch (updateAS)
		{
		case false:
			logString << "Allow Update AS OFF --------\n";
			break;
		case true:
			logString << "Allow AS ON --------\n";
			break;
		}

		break;
	}


	PerfWriter::Instance().Write(logString.str());

	//Init DXR
	fallbackContext = std::make_unique<DXRaytracingFallback>(testDurationInSec, model, rebuildAS, buildTypeAS, updateAS);
	fallbackContext->Initialize(hwnd, RESWIDTH, RESHEIGHT, windowTitle);


	//Main loop
	MSG msg = {};
	while (msg.message != WM_QUIT && !fallbackContext->TestDone())
	{
		//Process messages in the queue
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	//Stop other tests from executing
	if (msg.message == WM_QUIT)
	{
		closing = true;
	}

	//Destroy DXR
	fallbackContext->OnDestroy();
}

//Parses the given command line arguments
_Use_decl_annotations_
void SystemContext::ParseCommandLineArgs(WCHAR* argv[], int argc)
{
	for (int i = 1; i < argc; ++i)
	{
		// -fullscreen
		if (_wcsnicmp(argv[i], L"-fullscreen", wcslen(argv[i])) == 0 ||
			_wcsnicmp(argv[i], L"/fullscreen", wcslen(argv[i])) == 0)
		{
			fullScreen = true;
		}

		//... Add more?
	}
}

//Accepts the windows system messages and handles them. eg keyboard presses.
LRESULT SystemContext::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
	switch (umsg)
	{

		//Check if a key has been pressed on the keyboard
	case WM_KEYDOWN:
	{
		//OnKeyDown((UINT)wparam);
		return 0;
	}

	//Check if a key has been released on the keyboard
	case WM_KEYUP:
	{
		//OnKeyUp((UINT)wparam);
		return 0;
	}
	case WM_PAINT:
	{
		if (fallbackContext)
		{
			fallbackContext->Update();
			fallbackContext->Render();
		}

		return 0;
	}
	case WM_SIZE:
	{
		//if (fallbackContext)
		//{
		//	RECT clientRect = {};
		//	GetClientRect(hwnd, &clientRect);
		//	fallbackContext->OnSizeChanged(clientRect.right - clientRect.left, clientRect.bottom - clientRect.top, wparam == SIZE_MINIMIZED);
		//}
		return 0;
	}
	//Any other messages send to the default message handler as this application won't make use of them
	default:
	{
		return DefWindowProc(hwnd, umsg, wparam, lparam);
	}

	}
}

void SystemContext::OnKeyDown(UINT key)
{
	switch (key)
	{
	case 49:  //1
	case 50: //2
	case 87: //w
	case 83: //s
	case 65: //a
	case 68: //d
	case 37: //Left
	case 38: //Up
	case 39: //Right
	case 40: //Down
	case 17: //Ctrl
	case 32: //Space
		fallbackContext->HandleInput(key);
		break;
	default:
		break;
	}

}

void SystemContext::OnKeyUp(UINT key)
{
}

