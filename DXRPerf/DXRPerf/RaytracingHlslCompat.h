//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

#ifndef RAYTRACINGHLSLCOMPAT_H
#define RAYTRACINGHLSLCOMPAT_H




#ifdef HLSL
typedef float3 XMFLOAT3;
typedef float4 XMFLOAT4;
typedef float4 XMVECTOR;
typedef float4x4 XMMATRIX;
typedef uint UINT;
#else

using namespace DirectX;

// Shader will use byte encoding to access indices.
typedef UINT32 Index;
#endif

// Workaround for NV driver not supporting null local root signatures. 
// Use an empty local root signature where a shader does not require it.
#define USE_NON_NULL_LOCAL_ROOT_SIG 1

// PERFORMANCE TIP: Set max recursion depth as low as needed as drivers may apply optimization strategies for low recursion depths.
#define MAX_RAY_RECURSION_DEPTH 3    // ~ primary rays + reflections + shadow rays from reflected geometry.

#define RESWIDTH 1280
#define RESHEIGHT 720

static const XMFLOAT4 BackgroundColor = XMFLOAT4(0.0f, 0.2f, 0.4f, 1.0f);
static const XMFLOAT4 Albedo = XMFLOAT4(0.549f, 0.556f, 0.554f, 1.0f); //Chromium

static const float InShadowRadiance = 0.35f;

struct RayPayload
{
	XMFLOAT4 color;
	UINT   recursionDepth;
};

struct ShadowRayPayload
{
	bool hit;
};

struct SceneConstantBuffer
{
	XMMATRIX projectionToWorld;
	XMVECTOR cameraPosition;
	XMVECTOR lightPosition;
	XMVECTOR lightAmbientColor;
	XMVECTOR lightDiffuseColor;
};

struct ModelConstantBuffer
{
	XMFLOAT4 albedo;
};

struct Vertex
{
	XMFLOAT3 position;
	XMFLOAT3 normal;
};

struct BounceMap
{
	XMFLOAT3 locations[RESWIDTH][RESHEIGHT];
	XMFLOAT3 normals[RESWIDTH][RESWIDTH];
};

// Ray types traced
namespace RayType {
	enum Enum {
		Radiance = 0,   // ~ Primary, reflected camera/view rays calculating color for each hit.
		Shadow,         // ~ Shadow/visibility rays, only testing for occlusion
		Count
	};
}

namespace TraceRayParameters
{
	static const UINT InstanceMask = ~0;   // Everything is visible.
	namespace HitGroup {
		static const UINT Offset[RayType::Count] =
		{
			0, // Radiance ray
			1  // Shadow ray
		};
		// ToDo For now all geometries reusing shader records
		static const UINT GeometryStride = 0;// RayType::Count;
	}
	namespace MissShader {
		static const UINT Offset[RayType::Count] =
		{
			0, // Radiance ray
			1  // Shadow ray
		};
	}
}

#endif // RAYTRACINGHLSLCOMPAT_H