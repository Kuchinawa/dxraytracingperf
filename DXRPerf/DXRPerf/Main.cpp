#include "precomp.h"

#include "SystemContext.h"


//Function prototypes
static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

std::unique_ptr<SystemContext> window;

_Use_decl_annotations_
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR args, int nCmdShow)
{
	printf(args);
	window = std::make_unique<SystemContext>(RESWIDTH, RESHEIGHT, L"DXRPerf");
	window->Run(hInstance, nCmdShow, WndProc);
	return 0;
}

//Message handler for the window
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

	switch (message)
	{

		//Check if the window is being destroyed
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		return 0;
	}

	//Check if the window is being closed
	case WM_CLOSE:
	{
		PostQuitMessage(0);
		return 0;
	}

	}

	//Handle any messages the switch statement didn't.
	return window->MessageHandler(hWnd, message, wParam, lParam);
}
