#include "precomp.h"
#include "DXContext.h"

using namespace std;

using Microsoft::WRL::ComPtr;

DXContext::DXContext()
{
}


DXContext::~DXContext()
{
	WaitForGpu();
}

//Stores the window handle
void DXContext::SetWindow(HWND window, int width, int height, std::wstring windowName)
{
	windowHandle = window;
	windowTitle = windowName;

	this->width = width;
	this->height = height;

	outputSize.left = outputSize.top = 0;
	outputSize.right = width;
	outputSize.bottom = height;
}

//Setup DXGraphicsInfrastructure Factory and adapter
void DXContext::InitializeDXGIAdapter()
{

	bool debugDXGI = false;

#ifdef _DEBUG
	//Enable the debug layer (requires the "Graphics Tools" optional feature)
	//https://docs.microsoft.com/en-us/windows/uwp/gaming/use-the-directx-runtime-and-visual-studio-graphics-diagnostic-features
	{
		ComPtr<ID3D12Debug> debugController;
		if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController))))
		{
			debugController->EnableDebugLayer();
		}
		else
		{
			OutputDebugStringA("WARNING: Direct3D Debug Device is not available\n");
		}

		ComPtr<IDXGIInfoQueue> dxgiInfoQueue;
		if (SUCCEEDED(DXGIGetDebugInterface1(0, IID_PPV_ARGS(&dxgiInfoQueue))))
		{
			debugDXGI = true;

			ThrowIfFailed(CreateDXGIFactory2(DXGI_CREATE_FACTORY_DEBUG, IID_PPV_ARGS(&dxgiFactory)));

			dxgiInfoQueue->SetBreakOnSeverity(DXGI_DEBUG_ALL, DXGI_INFO_QUEUE_MESSAGE_SEVERITY_ERROR, true);
			dxgiInfoQueue->SetBreakOnSeverity(DXGI_DEBUG_ALL, DXGI_INFO_QUEUE_MESSAGE_SEVERITY_CORRUPTION, true);
		}
	}
#endif

	if (!debugDXGI)
	{
		ThrowIfFailed(CreateDXGIFactory1(IID_PPV_ARGS(&dxgiFactory)));
	}

	//Check for tearing support (VSYNC off)
	if (!VSYNC)
	{
		BOOL allowTearing = FALSE;

		ComPtr<IDXGIFactory5> factory5;
		HRESULT hr = dxgiFactory.As(&factory5);
		if (SUCCEEDED(hr))
		{
			hr = factory5->CheckFeatureSupport(DXGI_FEATURE_PRESENT_ALLOW_TEARING, &allowTearing, sizeof(allowTearing));
		}

		if (FAILED(hr) || !allowTearing)
		{
			OutputDebugStringA("WARNING: Variable refresh rate displays are not supported. \n");
			ThrowIfFailed(false, L"ERROR: This program must be run on an OS with tearing support. \n");
		}
	}

	//Init Adapter
	ComPtr<IDXGIAdapter1> adapterInit;

	for (UINT adapterId = 0; DXGI_ERROR_NOT_FOUND != dxgiFactory->EnumAdapters1(adapterId, &adapterInit); adapterId++)
	{
		//Is this the requested adapter? (If there is one requested)
		if (forceAdapter != UINT_MAX && adapterId != forceAdapter)
		{
			continue;
		}

		DXGI_ADAPTER_DESC1 desc;
		ThrowIfFailed(adapterInit->GetDesc1(&desc));

		if (desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE)
		{
			//Don't select the basic render Driver adapter. (WARP)
			continue;
		}

		//Check if the adapter supports Direct3D 12.
		if (SUCCEEDED(D3D12CreateDevice(adapterInit.Get(), minFeatureLevel, __uuidof(ID3D12Device), nullptr)))
		{
			this->adapterID = adapterId;
			adapterDescription = desc.Description;

#ifdef _DEBUG
			wchar_t buff[256] = {};
			swprintf_s(buff, L"Direct3D Adapter (%u): VID:%04X, PID:%04X - %ls\n", adapterID, desc.VendorId, desc.DeviceId, desc.Description);
			OutputDebugStringW(buff);
#endif
			break;
		}
	}


#ifndef NDEBUG
	if (!adapterInit && forceAdapter == UINT_MAX)
	{
		// Try WARP12 instead
		if (FAILED(dxgiFactory->EnumWarpAdapter(IID_PPV_ARGS(&adapterInit))))
		{
			throw exception("WARP12 not available. Enable the 'Graphics Tools' optional feature");
		}

		OutputDebugStringA("Direct3D Adapter - WARP12\n");
	}
#endif

	if (!adapterInit)
	{
		if (forceAdapter != UINT_MAX)
		{
			throw exception("Unavailable adapter requested.");
		}
		else
		{
			throw exception("Unavailable adapter.");
		}
	}

	adapter = adapterInit.Detach();
}

//Enable the experimental features for D3D Raytracing
void DXContext::EnableDXRExperimentalFeatures()
{
	// DXR is an experimental feature and needs to be enabled before creating a D3D12 device.
	bool DXRSupported = false;

	{
		//Device support
		ComPtr<ID3D12Device> testDevice;
		ComPtr<ID3D12DeviceRaytracingPrototype> testRaytracingDevice;
		UUID experimentalFeatures[] = { D3D12ExperimentalShaderModels, D3D12RaytracingPrototype };


		DXRSupported = SUCCEEDED(D3D12EnableExperimentalFeatures(2, experimentalFeatures, nullptr, nullptr))
			&& SUCCEEDED(D3D12CreateDevice(adapter.Get(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&testDevice)))
			&& SUCCEEDED(testDevice->QueryInterface(IID_PPV_ARGS(&testRaytracingDevice)));
	}

	if (!DXRSupported)
	{
		//Compute based fallback layer
		UUID experimentalFeatures[] = { D3D12ExperimentalShaderModels };   

		ComPtr<ID3D12Device> testDevice;

		ThrowIfFailed(D3D12EnableExperimentalFeatures(1, experimentalFeatures, nullptr, nullptr), L"Could not enable compute based fallback raytracing support (D3D12EnableExperimentalFeatures() failed, is developer mode on?).\n"); //Fallback layer
		ThrowIfFailed(D3D12CreateDevice(adapter.Get(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&testDevice)));

		OutputDebugString(L"Enabled compute based fallback raytracing support.\n");

		PerfWriter::Instance().Write("Using compute based path.\n");
	}
	else
	{
		OutputDebugString(L"DXR supported and enabled.\n");

		PerfWriter::Instance().Write("Using DXR based path.\n");
	}
}

//Create the device, descriptor heaps, fence, and message queue
void DXContext::CreateDevice()
{
	//Create the DX12 API device
	ThrowIfFailed(D3D12CreateDevice(adapter.Get(), minFeatureLevel, IID_PPV_ARGS(&d3dDevice)));

#ifndef NDEBUG

	//Configure debug device.
	ComPtr<ID3D12InfoQueue> d3dInfoQueue;
	if (SUCCEEDED(d3dDevice.As(&d3dInfoQueue)))
	{
#ifdef  _DEBUG
		d3dInfoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_CORRUPTION, true);
		d3dInfoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_ERROR, true);
#endif //  _DEBUG

		//Disable warnings that occur when using the VS Integrated graphics debugger
		D3D12_MESSAGE_ID hide[] =
		{
			D3D12_MESSAGE_ID_MAP_INVALID_NULLRANGE,
			D3D12_MESSAGE_ID_UNMAP_INVALID_NULLRANGE
		};
		D3D12_INFO_QUEUE_FILTER filter = {};

		filter.DenyList.NumIDs = _countof(hide);
		filter.DenyList.pIDList = hide;
		d3dInfoQueue->AddStorageFilterEntries(&filter);
	}
#endif // !NDEBUG

	//Determine the highest supported feature level for this device
	static const D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_12_1,
		D3D_FEATURE_LEVEL_12_0,
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
	};

	D3D12_FEATURE_DATA_FEATURE_LEVELS featLevels =
	{
		_countof(featureLevels), featureLevels, D3D_FEATURE_LEVEL_11_0
	};

	HRESULT hr = d3dDevice->CheckFeatureSupport(D3D12_FEATURE_FEATURE_LEVELS, &featLevels, sizeof(featLevels));
	if (SUCCEEDED(hr))
	{
		d3dFeatureLevel = featLevels.MaxSupportedFeatureLevel;
	}
	else
	{
		d3dFeatureLevel = minFeatureLevel;
	}

	//Create the command queue
	D3D12_COMMAND_QUEUE_DESC queueDesc = {};
	queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;

	ThrowIfFailed(d3dDevice->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&commandQueue)));

	//Create the descriptor heaps for the render target views and depth stencil views
	D3D12_DESCRIPTOR_HEAP_DESC rtvDescriptorHeapDesc = {};
	rtvDescriptorHeapDesc.NumDescriptors = backBufferCount;
	rtvDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;

	ThrowIfFailed(d3dDevice->CreateDescriptorHeap(&rtvDescriptorHeapDesc, IID_PPV_ARGS(&rtvDescriptorHeap)));
	rtvDescriptorSize = d3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

	if (depthBufferFormat != DXGI_FORMAT_UNKNOWN)
	{
		D3D12_DESCRIPTOR_HEAP_DESC dsvDescriptorHeapDesc = {};
		dsvDescriptorHeapDesc.NumDescriptors = 1;
		dsvDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;

		ThrowIfFailed(d3dDevice->CreateDescriptorHeap(&dsvDescriptorHeapDesc, IID_PPV_ARGS(&dsvDescriptorHeap)));
	}

	//Create command allocator for each back buffer that will be rendered to
	for (UINT n = 0; n < backBufferCount; n++)
	{
		ThrowIfFailed(d3dDevice->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&commandAllocators[n])));
	}

	// Create a command list for recording graphics commands
	ThrowIfFailed(d3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, commandAllocators[0].Get(), nullptr, IID_PPV_ARGS(&commandList)));
	ThrowIfFailed(commandList->Close());

	// Create a fence for tracking GPU execution progress.
	ThrowIfFailed(d3dDevice->CreateFence(fenceValues[backBufferIndex], D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence)));
	fenceValues[backBufferIndex]++;

	fenceEvent.Attach(CreateEvent(nullptr, FALSE, FALSE, nullptr));
	if (!fenceEvent.IsValid())
	{
		ThrowIfFailed(E_FAIL, L"CreateEvent failed.\n");
	}
}

//These resources need to be recreated every time the window size changes
void DXContext::CreateWindowSizeDependentResources()
{
	if (!windowHandle)
	{
		ThrowIfFailed(E_HANDLE, L"Call SetWindow with a valid Win32 window handle.\n");
	}

	//Wait until all previous GPU work is complete
	WaitForGpu();

	//Release resources that are tied to the swap chain and update fence values.
	for (UINT n = 0; n < backBufferCount; n++)
	{
		renderTargets[n].Reset();
		fenceValues[n] = fenceValues[backBufferIndex];
	}

	//Determine the render target size in pixels
	UINT backBufferWidth = max(outputSize.right - outputSize.left, 1);
	UINT backBufferHeight = max(outputSize.bottom - outputSize.top, 1);

	//If the swap chain already exists, resize it, otherwise create one
	if (swapChain)
	{
		//swapchain exists, resize it
		HRESULT hr = swapChain->ResizeBuffers(
			backBufferCount,
			backBufferWidth,
			backBufferHeight,
			backBufferFormat,
			VSYNC ? 0 : DXGI_SWAP_CHAIN_FLAG_ALLOW_TEARING /*VSYNC OFF*/);

		if (hr == DXGI_ERROR_DEVICE_REMOVED || hr == DXGI_ERROR_DEVICE_RESET)
		{
#ifdef _DEBUG
			char buff[64] = {};
			sprintf_s(buff, "Device Lost on ResizeBuffers: Reason code 0x%08X\n", (hr == DXGI_ERROR_DEVICE_REMOVED) ? d3dDevice->GetDeviceRemovedReason() : hr);
			OutputDebugStringA(buff);
#endif

			//If the device was removed, a new device and swap chain need to be created
			HandleDeviceLost();

			//Everything is set up now. Do not continue execution of this method. HandleDeviceLost will reenter this method 
			//and correctly set up the new device.
			return;
		}
		else
		{
			ThrowIfFailed(hr);
		}
	}
	else
	{
		//Create the swap chain descriptor
		DXGI_SWAP_CHAIN_DESC1 swapChainDesc = {};
		swapChainDesc.Width = backBufferWidth;
		swapChainDesc.Height = backBufferHeight;
		swapChainDesc.Format = backBufferFormat;
		swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		swapChainDesc.BufferCount = backBufferCount;
		swapChainDesc.SampleDesc.Count = 1;
		swapChainDesc.SampleDesc.Quality = 0;
		swapChainDesc.Scaling = DXGI_SCALING_STRETCH;
		swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
		swapChainDesc.AlphaMode = DXGI_ALPHA_MODE_IGNORE;
		swapChainDesc.Flags = VSYNC ? 0 : DXGI_SWAP_CHAIN_FLAG_ALLOW_TEARING; //VSYNC OFF

		DXGI_SWAP_CHAIN_FULLSCREEN_DESC fsSwapChainDesc = { 0 };
		fsSwapChainDesc.Windowed = TRUE;

		//Create the swap chain
		ComPtr<IDXGISwapChain1> swapChainInit;

		// DXGI does not allow creating a swapchain targeting a window which has fullscreen styles(no border + topmost).
		// Temporarily remove the topmost property for creating the swapchain.
		bool prevIsFullscreen = isFullscreen;
		if (prevIsFullscreen)
		{
			RECT windowRect;
			GetWindowRect(windowHandle, &windowRect);

			SetWindowPos(
				windowHandle,
				HWND_NOTOPMOST,
				windowRect.left,
				windowRect.top,
				windowRect.right - windowRect.left,
				windowRect.bottom - windowRect.top,
				SWP_FRAMECHANGED | SWP_NOACTIVATE);
		}

		ThrowIfFailed(dxgiFactory->CreateSwapChainForHwnd(commandQueue.Get(), windowHandle, &swapChainDesc, &fsSwapChainDesc, nullptr, &swapChainInit));

		if (prevIsFullscreen)
		{
			RECT windowRect;
			GetWindowRect(windowHandle, &windowRect);

			SetWindowPos(
				windowHandle,
				HWND_TOPMOST,
				windowRect.left,
				windowRect.top,
				windowRect.right - windowRect.left,
				windowRect.bottom - windowRect.top,
				SWP_FRAMECHANGED | SWP_NOACTIVATE);
		}

		ThrowIfFailed(swapChainInit.As(&swapChain));

		if (!VSYNC)
		{
			dxgiFactory->MakeWindowAssociation(windowHandle, DXGI_MWA_NO_ALT_ENTER);
		}
	}

	//Obtain the backbuffers for the window which will be the final render targets, and create render target views for each
	for (UINT n = 0; n < backBufferCount; n++)
	{
		ThrowIfFailed(swapChain->GetBuffer(n, IID_PPV_ARGS(&renderTargets[n])));

		wchar_t name[25] = {};
		swprintf_s(name, L"Render target %u", n);
		renderTargets[n]->SetName(name);

		D3D12_RENDER_TARGET_VIEW_DESC rtvDesc = {};
		rtvDesc.Format = backBufferFormat;
		rtvDesc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;

		CD3DX12_CPU_DESCRIPTOR_HANDLE rtvDescriptor(rtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart(), n, rtvDescriptorSize);
		d3dDevice->CreateRenderTargetView(renderTargets[n].Get(), &rtvDesc, rtvDescriptor);
	}

	//Reset the index to the current back buffer
	backBufferIndex = swapChain->GetCurrentBackBufferIndex();

	if (depthBufferFormat != DXGI_FORMAT_UNKNOWN)
	{
		//Allocate a 2-D surface as the depth/stencil buffer and create a depth/stencil view on this surface.
		CD3DX12_HEAP_PROPERTIES depthHeapProperties(D3D12_HEAP_TYPE_DEFAULT);

		D3D12_RESOURCE_DESC depthStencilDesc = CD3DX12_RESOURCE_DESC::Tex2D(
			depthBufferFormat,
			backBufferWidth,
			backBufferHeight,
			1, //This depth stencil view has 1 texture
			1  //Use a single mipmap level
		);

		depthStencilDesc.Flags |= D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

		D3D12_CLEAR_VALUE depthOptimizedClearValue = {};
		depthOptimizedClearValue.Format = depthBufferFormat;
		depthOptimizedClearValue.DepthStencil.Depth = 1.0f;
		depthOptimizedClearValue.DepthStencil.Stencil = 0;

		ThrowIfFailed(d3dDevice->CreateCommittedResource(&depthHeapProperties,
			D3D12_HEAP_FLAG_NONE,
			&depthStencilDesc,
			D3D12_RESOURCE_STATE_DEPTH_WRITE,
			&depthOptimizedClearValue,
			IID_PPV_ARGS(&depthStencil)
		));

		depthStencil->SetName(L"Depth Stencil");

		D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc = {};
		dsvDesc.Format = depthBufferFormat;
		dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;

		d3dDevice->CreateDepthStencilView(depthStencil.Get(), &dsvDesc, dsvDescriptorHeap->GetCPUDescriptorHandleForHeapStart());
	}

	//Set the 3D rendering viewport and scissor rectangle to target the entire window
	screenViewport.TopLeftX = screenViewport.TopLeftY = 0.f;
	screenViewport.Width = static_cast<float>(backBufferWidth);
	screenViewport.Height = static_cast<float>(backBufferHeight);
	screenViewport.MinDepth = D3D12_MIN_DEPTH;
	screenViewport.MaxDepth = D3D12_MAX_DEPTH;

	//Scissor rect is used to cull out of screen pixels
	scissorRect.left = scissorRect.top = 0;
	scissorRect.right = backBufferWidth;
	scissorRect.bottom = backBufferHeight;
}

// Recreate all device resources and set them back to the current state.
void DXContext::HandleDeviceLost()
{
	if (deviceNotify)
	{
		deviceNotify->OnDeviceLost();
	}

	for (UINT n = 0; n < backBufferCount; n++)
	{
		commandAllocators[n].Reset();
		renderTargets[n].Reset();
	}

	depthStencil.Reset();
	commandQueue.Reset();
	commandList.Reset();
	fence.Reset();
	rtvDescriptorHeap.Reset();
	dsvDescriptorHeap.Reset();
	swapChain.Reset();
	d3dDevice.Reset();
	dxgiFactory.Reset();
	adapter.Reset();

#ifdef _DEBUG
	{
		ComPtr<IDXGIDebug1> dxgiDebug;
		if (SUCCEEDED(DXGIGetDebugInterface1(0, IID_PPV_ARGS(&dxgiDebug))))
		{
			dxgiDebug->ReportLiveObjects(DXGI_DEBUG_ALL, DXGI_DEBUG_RLO_FLAGS(DXGI_DEBUG_RLO_SUMMARY | DXGI_DEBUG_RLO_IGNORE_INTERNAL));
		}
	}
#endif
	InitializeDXGIAdapter();
	CreateDevice();
	CreateWindowSizeDependentResources();

	if (deviceNotify)
	{
		deviceNotify->OnDeviceRestored();
	}
}

// Wait for pending GPU work to complete.
void DXContext::WaitForGpu() noexcept
{
	if (commandQueue && fence && fenceEvent.IsValid())
	{
		// Schedule a Signal command in the GPU queue.
		UINT64 fenceValue = fenceValues[backBufferIndex];
		if (SUCCEEDED(commandQueue->Signal(fence.Get(), fenceValue)))
		{
			// Wait until the Signal has been processed.
			if (SUCCEEDED(fence->SetEventOnCompletion(fenceValue, fenceEvent.Get())))
			{
				WaitForSingleObjectEx(fenceEvent.Get(), INFINITE, FALSE);

				// Increment the fence value for the current frame.
				fenceValues[backBufferIndex]++;
			}
		}
	}
}

//Send the command list off to the GPU for processing
void DXContext::ExecuteCommandList()
{
	ThrowIfFailed(commandList->Close());
	ID3D12CommandList* commandLists[] = { commandList.Get() };
	commandQueue->ExecuteCommandLists(ARRAYSIZE(commandLists), commandLists);
}

void DXContext::UpdateForSizeChange(UINT newWidth, UINT newHeight)
{
	width = newWidth;
	height = newHeight;

	aspectRatio = static_cast<float>(newHeight) / static_cast<float>(newHeight);
}


void DXContext::ResetCommandAllocatorAndCommandlist()
{
	//Reset command list and allocator
	ThrowIfFailed(commandAllocators[backBufferIndex]->Reset());
	ThrowIfFailed(commandList->Reset(commandAllocators[backBufferIndex].Get(), nullptr));
}


//Prepare the command list and render target for rendering
void DXContext::Prepare(D3D12_RESOURCE_STATES beforeState)
{
	//Reset command list and allocator
	ResetCommandAllocatorAndCommandlist();

	if (beforeState != D3D12_RESOURCE_STATE_RENDER_TARGET)
	{
		//Transition the render target into the correct state to allow for drawing into it
		D3D12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(renderTargets[backBufferIndex].Get(), beforeState, D3D12_RESOURCE_STATE_RENDER_TARGET);
		commandList->ResourceBarrier(1, &barrier);
	}
}

//Present the contents of the swap chain to the screen
void DXContext::Present(D3D12_RESOURCE_STATES beforeState)
{
	if (beforeState != D3D12_RESOURCE_STATE_PRESENT)
	{
		//Transition the render target to the state that allows it to be presented to the display
		D3D12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(renderTargets[backBufferIndex].Get(), beforeState, D3D12_RESOURCE_STATE_PRESENT);
		commandList->ResourceBarrier(1, &barrier);
	}

	ExecuteCommandList();

	HRESULT hResult;
	if (!VSYNC)
	{
		hResult = swapChain->Present(0, DXGI_PRESENT_ALLOW_TEARING); //VSync off
	}
	else
	{
		//The first argument instructs DXGI to block until VSync
		hResult = swapChain->Present(1, 0);
	}

	//If the device was reset we must completely reinitialize the renderer
	if (hResult == DXGI_ERROR_DEVICE_REMOVED || hResult == DXGI_ERROR_DEVICE_RESET)
	{
#ifdef _DEBUG
		char buff[64] = {};
		sprintf_s(buff, "Device Lost on Present: Reason code 0x%08X\n", (hResult == DXGI_ERROR_DEVICE_REMOVED) ? d3dDevice->GetDeviceRemovedReason() : hResult);
		OutputDebugStringA(buff);
#endif
		HandleDeviceLost();
	}
	else
	{
		ThrowIfFailed(hResult);

		MoveToNextFrame();
	}
}

void DXContext::OnDestroy()
{
}

bool DXContext::OnSizeChanged(UINT width, UINT height, bool minimized)
{
	this->minimized = minimized;

	if (minimized || width == 0 || height == 0)
	{
		return false;
	}

	RECT newRc;
	newRc.left = newRc.top = 0;
	newRc.right = width;
	newRc.bottom = height;
	if (newRc.left == outputSize.left
		&& newRc.top == outputSize.top
		&& newRc.right == outputSize.right
		&& newRc.bottom == outputSize.bottom)
	{
		//No changes, return
		return false;
	}

	outputSize = newRc;
	CreateWindowSizeDependentResources();
	ResizeWindow(width, height);

	return true;


}

//Change the title text next to the window title
void DXContext::SetTitle(LPCWSTR text)
{
	std::wstring newTitle = windowTitle + L": " + text;
	SetWindowText(windowHandle, newTitle.c_str());
}

//Prepare to render the next frame
void DXContext::MoveToNextFrame()
{
	//Schedule a signal command in the queue
	const UINT64 currentFenceValue = fenceValues[backBufferIndex];
	ThrowIfFailed(commandQueue->Signal(fence.Get(), currentFenceValue));

	//Update the back buffer index
	backBufferIndex = swapChain->GetCurrentBackBufferIndex();

	//If the next frame is not ready to be rendered yet, wait until it is read
	if (fence->GetCompletedValue() < fenceValues[backBufferIndex])
	{
		ThrowIfFailed(fence->SetEventOnCompletion(fenceValues[backBufferIndex], fenceEvent.Get()));
		WaitForSingleObjectEx(fenceEvent.Get(), INFINITE, FALSE);
	}

	//Set the fence value for the next frame
	fenceValues[backBufferIndex] = currentFenceValue + 1;
}

void DXContext::ResizeWindow(UINT width, UINT height)
{
	this->width = width;
	this->height = height;

	this->aspectRatio = static_cast<float>(width) / static_cast<float>(height);
}
