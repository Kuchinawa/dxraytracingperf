//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

#ifndef RAYTRACING_HLSL
#define RAYTRACING_HLSL

#define HLSL
#include "RaytracingHlslCompat.h"
#include "RaytracingShaderUtil.hlsli"

RaytracingAccelerationStructure Scene : register(t0, space0);
RWTexture2D<float4> RenderTarget : register(u0);
ByteAddressBuffer Indices : register(t1, space0);
StructuredBuffer<Vertex> Vertices : register(t2, space0);

RWBuffer<float4> bounceVertexMap : register(u1);
RWBuffer<float4> bounceNormalMap : register(u2);

ConstantBuffer<SceneConstantBuffer> g_sceneCB : register(b0);
ConstantBuffer<ModelConstantBuffer> g_modelCB : register(b1);

typedef BuiltInTriangleIntersectionAttributes TriangleAttributes;

// Diffuse lighting calculation.
float CalculateDiffuseCoefficient(in float3 hitPosition, in float3 incidentLightRay, in float3 normal)
{
	float fNDotL = saturate(dot(-incidentLightRay, normal));
	return fNDotL;
}

// Phong lighting specular component
float4 CalculateSpecularCoefficient(in float3 hitPosition, in float3 incidentLightRay, in float3 normal, in float specularPower)
{
	float3 reflectedLightRay = normalize(reflect(incidentLightRay, normal));
	return pow(saturate(dot(reflectedLightRay, normalize(-WorldRayDirection()))), specularPower);
}

// Phong lighting model = ambient + diffuse + specular components.
float4 CalculatePhongLighting(in float4 albedo, in float3 normal, in bool isInShadow, in float diffuseCoef = 1.0, in float specularCoef = 1.0, in float specularPower = 50)
{
	float3 hitPosition = HitWorldPosition();
	float3 lightPosition = g_sceneCB.lightPosition.xyz;
	float shadowFactor = isInShadow ? InShadowRadiance : 1.0;
	float3 incidentLightRay = normalize(hitPosition - lightPosition);

	// Diffuse component.
	float4 lightDiffuseColor = g_sceneCB.lightDiffuseColor;
	float Kd = CalculateDiffuseCoefficient(hitPosition, incidentLightRay, normal);
	float4 diffuseColor = shadowFactor * diffuseCoef * Kd * lightDiffuseColor * albedo;

	// Specular component.
	float4 specularColor = float4(0, 0, 0, 0);
	if (!isInShadow)
	{
		float4 lightSpecularColor = float4(1, 1, 1, 1);
		float4 Ks = CalculateSpecularCoefficient(hitPosition, incidentLightRay, normal, specularPower);
		specularColor = specularCoef * Ks * lightSpecularColor;
	}

	// Ambient component.
	float4 ambientColor = g_sceneCB.lightAmbientColor * albedo;

	return ambientColor + diffuseColor + specularColor;
}

//Trace a radiance ray into the scene and return a shaded color
float4 TraceRadianceRay(in Ray ray, in UINT currentRayRecursionDepth)
{
	if (currentRayRecursionDepth >= MAX_RAY_RECURSION_DEPTH)
	{
		return float4(0, 0, 0, 0);
	}

	//Set the ray's extents.
	RayDesc rayDesc;
	rayDesc.Origin = ray.origin;
	rayDesc.Direction = ray.direction;

	rayDesc.TMin = 0.001;
	rayDesc.TMax = 10000;

	RayPayload rayPayload = { float4(0, 0, 0, 0), currentRayRecursionDepth + 1 };
	TraceRay(Scene,
		RAY_FLAG_CULL_BACK_FACING_TRIANGLES,
		TraceRayParameters::InstanceMask,
		TraceRayParameters::HitGroup::Offset[RayType::Radiance],
		TraceRayParameters::HitGroup::GeometryStride,
		TraceRayParameters::MissShader::Offset[RayType::Radiance],
		rayDesc, rayPayload);

	return rayPayload.color;
}

// Trace a shadow ray and return true if it hits any geometry.
bool TraceShadowRayAndReportIfHit(in Ray ray, in UINT currentRayRecursionDepth)
{
	if (currentRayRecursionDepth >= MAX_RAY_RECURSION_DEPTH)
	{
		return false;
	}

	// Set the ray's extents.
	RayDesc rayDesc;
	rayDesc.Origin = ray.origin;
	rayDesc.Direction = ray.direction;
	// Set TMin to a zero value to avoid aliasing artifcats along contact areas.
	// Note: make sure to enable back-face culling so as to avoid surface face fighting.
	rayDesc.TMin = 0.001;
	rayDesc.TMax = 10000;	// ToDo set this to dist to light

							// Initialize shadow ray payload.
							// Set the initial value to true since closest and any hit shaders are skipped. 
							// Shadow miss shader, if called, will set it to false.
	ShadowRayPayload shadowPayload = { true };
	TraceRay(Scene,
		RAY_FLAG_CULL_BACK_FACING_TRIANGLES
		| RAY_FLAG_ACCEPT_FIRST_HIT_AND_END_SEARCH
		| RAY_FLAG_FORCE_OPAQUE             // ~skip any hit shaders
		| RAY_FLAG_SKIP_CLOSEST_HIT_SHADER, // ~skip closest hit shaders,
		TraceRayParameters::InstanceMask,
		TraceRayParameters::HitGroup::Offset[RayType::Shadow],
		TraceRayParameters::HitGroup::GeometryStride,
		TraceRayParameters::MissShader::Offset[RayType::Shadow],
		rayDesc, shadowPayload);

	return shadowPayload.hit;
}

[shader("raygeneration")]
void MyRaygenShader()
{
	//Generate a ray through a camera pixel
	Ray ray = GenerateCameraRay(DispatchRaysIndex().xy, g_sceneCB.cameraPosition.xyz, g_sceneCB.projectionToWorld);

	//Cast ray into the scene, and retrieve a shaded color
	UINT currentRecursionDepth = 0;
	float4 color = TraceRadianceRay(ray, currentRecursionDepth);

	// Write the raytraced color to the output texture.
	RenderTarget[DispatchRaysIndex().xy] = color;
}

[shader("closesthit")]
void MyClosestHitShader(inout RayPayload payload, in TriangleAttributes attr)
{
	// Get the base index of the triangle's first 32 bit index.
	uint indexSizeInBytes = 4;
	uint indicesPerTriangle = 3;
	uint triangleIndexStride = indicesPerTriangle * indexSizeInBytes;
	uint baseIndex = PrimitiveIndex() * triangleIndexStride;

	const uint3 indices = Load3x32BitIndices(baseIndex, Indices);

	// Retrieve corresponding vertex normals for the triangle vertices.
	float3 vertexNormals[3] = {
		Vertices[indices[0]].normal,
		Vertices[indices[1]].normal,
		Vertices[indices[2]].normal
	};

	float3 triangleNormal = HitAttribute(vertexNormals, attr);

	//Trace a shadow ray.
	float3 hitPosition = HitWorldPosition();
	Ray shadowRay = { hitPosition, normalize(g_sceneCB.lightPosition.xyz - hitPosition) };
	bool shadowRayHit = TraceShadowRayAndReportIfHit(shadowRay, payload.recursionDepth);

	//Reflection
	float4 reflectedColor = float4(0, 0, 0, 0);
	float reflectanceCoef = 1.0f;
	float diffuseCoef = 0.9f;
	float specularCoef = 0.7f;
	float specularPower = 50.0f;


	if (reflectanceCoef > 0.001)
	{
		//Trace a reflection ray
		Ray reflectionRay = { HitWorldPosition(), reflect(WorldRayDirection(), triangleNormal) };
		float4 reflectionColor = TraceRadianceRay(reflectionRay, payload.recursionDepth);

		float3 fresnelR = FresnelReflectanceSchlick(WorldRayDirection(), triangleNormal, g_modelCB.albedo.xyz);
		reflectedColor = reflectanceCoef * float4(fresnelR, 1) * reflectionColor;
	}

	// Calculate final color.
	float4 phongColor = CalculatePhongLighting(g_modelCB.albedo, triangleNormal, shadowRayHit, diffuseCoef, specularCoef, specularPower);
	float4 color = (phongColor + reflectedColor);

	// Apply visibility falloff.
	float t = RayTCurrent();
	color = lerp(color, BackgroundColor, 1.0 - exp(-0.000002*t*t*t));

	payload.color = color;
}

[shader("miss")]
void MyMissShader(inout RayPayload payload)
{
	float4 background = float4(BackgroundColor);
	payload.color = background;
}

[shader("miss")]
void MyMissShader_ShadowRay(inout ShadowRayPayload rayPayload)
{
	rayPayload.hit = false;
}

#endif // RAYTRACING_HLSL