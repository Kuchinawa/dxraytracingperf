#include "precomp.h"
#include "Model.h"

//Tiny obj loader defines, (only add in 1 file)
#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>

using std::vector;

Model::Model(const std::string modelPath)
{
	LoadModel(modelPath);
}


Model::~Model()
{
}

void Model::LoadModel(const std::string modelPath)
{
	tinyobj::attrib_t attrib;

	vector<tinyobj::shape_t> shapes;
	vector<tinyobj::material_t> materials;

	std::string error;

	//Load obj file into memory
	if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &error, modelPath.c_str(), nullptr)) //Triangulate is default true
	{
		throw std::runtime_error(error);
	}

	//Load the vertices
	vertices.reserve(attrib.vertices.size() / 3);
	indices.reserve(attrib.vertices.size() / 3);

	//Loop over faces
	for (const tinyobj::shape_t& shape : shapes)
	{
		for (size_t f = 0; f < shape.mesh.num_face_vertices.size(); f++)
		{

			//Loop over vertices
			for (size_t v = 0; v < 3; v++)
			{
				//Vertex index
				tinyobj::index_t idx = shape.mesh.indices[(f * 3) + v];

				XMFLOAT3 position =
				{
					attrib.vertices[3 * idx.vertex_index + 0],
					attrib.vertices[3 * idx.vertex_index + 1],
					attrib.vertices[3 * idx.vertex_index + 2]
				};

				XMFLOAT3 normal =
				{
					attrib.normals[3 * idx.normal_index + 0],
					attrib.normals[3 * idx.normal_index + 1],
					attrib.normals[3 * idx.normal_index + 2]
				};

				Vertex vertex = { position, normal };

				vertices.push_back(vertex);
				//Store this vertex index
				indices.push_back(indices.size());
			}

			triCount++;
		}
	}
}
