//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

#pragma once

//----------------------------------------------------------------------------------
// DirectX 12 implementation of GPU timer
class GPUTimer
{
public:
	static const size_t c_maxTimers = 8;

	GPUTimer() :
		m_gpuFreqInv(1.f),
		m_avg{},
		m_timing{}
	{}

	GPUTimer(ID3D12Device* device, ID3D12CommandQueue* commandQueue) :
		m_gpuFreqInv(1.f),
		m_avg{},
		m_timing{}
	{
		RestoreDevice(device, commandQueue);
	}

	GPUTimer(const GPUTimer&) = delete;
	GPUTimer& operator=(const GPUTimer&) = delete;

	GPUTimer(GPUTimer&&) = default;
	GPUTimer& operator=(GPUTimer&&) = default;

	~GPUTimer() { ReleaseDevice(); }

	// Indicate beginning & end of frame
	void BeginFrame(_In_ ID3D12GraphicsCommandList* commandList);
	void EndFrame(_In_ ID3D12GraphicsCommandList* commandList);

	// Start/stop a particular performance timer (don't start same index more than once in a single frame)
	void Start(_In_ ID3D12GraphicsCommandList* commandList, uint32_t timerid = 0);
	void Stop(_In_ ID3D12GraphicsCommandList* commandList, uint32_t timerid = 0);

	// Reset running average
	void Reset();

	// Returns delta time in milliseconds
	double GetElapsedMS(uint32_t timerid = 0) const;

	// Returns running average in milliseconds
	float GetAverageMS(uint32_t timerid = 0) const
	{
		return (timerid < c_maxTimers) ? m_avg[timerid] : 0.f;
	}

	// Device management
	void ReleaseDevice();
	void RestoreDevice(_In_ ID3D12Device* device, _In_ ID3D12CommandQueue* commandQueue);

private:
	static const size_t c_timerSlots = c_maxTimers * 2;

	Microsoft::WRL::ComPtr<ID3D12QueryHeap> m_heap;
	Microsoft::WRL::ComPtr<ID3D12Resource>  m_buffer;
	double                                  m_gpuFreqInv;
	float                                   m_avg[c_maxTimers];
	UINT64                                  m_timing[c_timerSlots];
};