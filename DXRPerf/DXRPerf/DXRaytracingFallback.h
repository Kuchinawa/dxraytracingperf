#pragma once

#include "RaytracingHlslCompat.h"

// Provides an interface for an application that owns DeviceResources to be notified of the device being lost or created.
interface IDeviceNotify
{
	virtual void OnDeviceLost() = 0;
	virtual void OnDeviceRestored() = 0;
};

namespace GlobalRootSignatureParams
{
	enum Value {
		OutputViewSlot = 0,
		AccelerationStructureSlot,
		SceneConstantSlot,
		VertexBuffersSlot,
		BounceMapSlot,
		Count
	};
}

namespace LocalRootSignatureParams
{
	enum Value {
		ModelConstantSlot = 0,
		Count
	};
}

enum ASBuildType
{
	FastTrace,
	FastBuild
};

class DXRaytracingFallback : public IDeviceNotify
{
public:
	DXRaytracingFallback(float testDurationInSec, std::shared_ptr<Model> model, bool ASRebuild = false, ASBuildType asBuildType = FastTrace, bool ASAllowUpdate = false);
	~DXRaytracingFallback();

	void Initialize(HWND window, int width, int height, std::wstring windowName);

	void Update();
	void Render();

	void OnDestroy();
	void OnSizeChanged(UINT width, UINT height, bool minimized);

	void HandleInput(UINT key);

	bool TestDone() { return testDone; };

private:

	//Settings
	UINT maxRecursionDepth = MAX_RAY_RECURSION_DEPTH; //PERFORMACE TIP: Set max recursion depth as low as needed as drivers may apply optimization strategies for low recursion depths.

	ASBuildType asBuildType = FastTrace; //Build AS for fast (re)build or fast trace
	bool ASAllowUpdate = true; //Update instead of rebuild (not supported by fallback layer)
	bool ASRebuild = false; //Force rebuild every frame
	bool ASReinit = false; //Flag to force reinitialization of AS
	GPUTimer ASRebuildTimer;

	bool testDone = false;
	float testDurationInSec;

	const UINT shaderCount = 6;
	UINT currentShaderIndex = 0;

	bool bouncesStored = false;
	UINT OAframeCount = 0;
	
	StepTimer timer;

	//Buffers for the bounce locations and normals
	Microsoft::WRL::ComPtr<ID3D12Resource> bounceMapVertexBuffer;
	D3D12_GPU_DESCRIPTOR_HANDLE bounceMapVertexUAVGpuDescriptor; //Handle to the output resource Unordered access view
	UINT bounceMapVertexUAVDescriptorHeapIndex = UINT_MAX;

	Microsoft::WRL::ComPtr<ID3D12Resource> bounceMapNormalBuffer;
	D3D12_GPU_DESCRIPTOR_HANDLE bounceMapNormalUAVGpuDescriptor; //Handle to the output resource Unordered access view
	UINT bounceMapNormalUAVDescriptorHeapIndex = UINT_MAX;

	//Buffers for the AO test data
	Microsoft::WRL::ComPtr<ID3D12Resource> AOTestBuffer;
	D3D12_GPU_DESCRIPTOR_HANDLE AOTestUAVGpuDescriptor; //Handle to the output resource Unordered access view
	UINT AOTestUAVDescriptorHeapIndex = UINT_MAX;


	Microsoft::WRL::ComPtr<ID3D12Resource> AOReadbackBuffer; //Destination buffer for CPU access

	void LoadBounceMap();
	void InitializeBumpMap();
	void InitializeAO();
	void ReadbackAOData();

	//Init stuff
	void InitializeScene();
	void UpdateCameraMatrices();
	void CreateAuxilaryDeviceResources();
	void CreateDeviceDependentResourcesForRT();
	void CreateRaytracingInterfaces();
	void CreateRootSignatures();
	void SerializeAndCreateRaytracingRootSignature(D3D12_ROOT_SIGNATURE_DESC& desc, Microsoft::WRL::ComPtr<ID3D12RootSignature>* rootSig);
	void CreateLocalRootSignatureSubobjects(CD3D12_STATE_OBJECT_DESC* raytracingPipeline);
	Microsoft::WRL::ComPtr<ID3D12RaytracingFallbackStateObject> CreateRaytracingPipelineStateObject(CD3DX12_SHADER_BYTECODE shaderCode, UINT shaderIndex);
	void CreateDescriptorHeap();
	void BuildGeometry();
	void BuildAccelerationStructures();
	void UpdateAccelerationStructures(bool forceRebuild, bool update);
	void CreateConstantBuffers();
	void BuildShaderTables(UINT shaderIndex);
	void CreateRaytracingOutputResource();
	void CreateWindowSizeDependentResourcesForRT();

	void UpdateForSizeChange(UINT newWidth, UINT newHeight);


	WRAPPED_GPU_POINTER CreateFallbackWrappedPointer(ID3D12Resource* resource, UINT bufferNumElements, UINT* descriptorHeapIndex);
	UINT AllocateDescriptor(D3D12_CPU_DESCRIPTOR_HANDLE* cpuDescriptor, UINT descriptorIndexToUse = UINT_MAX);
	UINT CreateBufferSRV(D3DBuffer* buffer, UINT numElements, UINT elementSize);

	//Release stuff
	void ReleaseWindowSizeDependentResourcesForRT();
	void ReleaseDeviceDependentResourcesForRT();


	//Per frame stuff
	void CalculatePerformance();

	void DoRaytracing();
	void CopyRaytracingOutputToBackbuffer();


	// Inherited via IDeviceNotify
	virtual void OnDeviceLost() override;
	virtual void OnDeviceRestored() override;

	//Standard directx context, with the experimental raytracing fallback features activated
	DXContext dxContext;

	// We'll allocate space for several of these and they will need to be padded for alignment.
	static_assert(sizeof(SceneConstantBuffer) < D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT, "Checking the size here.");

	union AlignedSceneConstantBuffer
	{
		SceneConstantBuffer constants;
		uint8_t alignmentPadding[D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT];
	};
	AlignedSceneConstantBuffer* mappedConstantData;
	Microsoft::WRL::ComPtr<ID3D12Resource> perFrameConstants;

	//Fallback layer attributes
	Microsoft::WRL::ComPtr<ID3D12RaytracingFallbackDevice> fallbackDevice;
	Microsoft::WRL::ComPtr<ID3D12RaytracingFallbackCommandList> fallbackCommandList;
	std::vector<Microsoft::WRL::ComPtr<ID3D12RaytracingFallbackStateObject>> fallbackStateObject;

	//Shader tables
	static const wchar_t* hitGroupName;
	static const wchar_t* raygenShaderName;
	static const wchar_t* closestHitShaderName;
	static const wchar_t* missShaderName;
	std::vector<Microsoft::WRL::ComPtr<ID3D12Resource>> missShaderTableResource;
	std::vector<Microsoft::WRL::ComPtr<ID3D12Resource>> hitGroupShaderTableResource;
	std::vector<Microsoft::WRL::ComPtr<ID3D12Resource>> rayGenShaderTableResource;

	// Root signatures
	Microsoft::WRL::ComPtr<ID3D12RootSignature> raytracingGlobalRootSignature;
	Microsoft::WRL::ComPtr<ID3D12RootSignature> raytracingLocalRootSignature;
#if USE_NON_NULL_LOCAL_ROOT_SIG 
	Microsoft::WRL::ComPtr<ID3D12RootSignature> raytracingLocalRootSignatureEmpty;
#endif

	// Descriptors
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> descriptorHeap;
	UINT descriptorsAllocated;
	UINT descriptorSize;


	//Camera state
	XMVECTOR cameraEye;
	XMVECTOR cameraLookAt;
	XMVECTOR cameraUp;

	// Raytracing scene
	SceneConstantBuffer sceneCB;
	ModelConstantBuffer modelCB;

	// Geometry
	std::shared_ptr<Model> sceneModel; //Own model class

	D3DBuffer indexBuffer;
	D3DBuffer vertexBuffer;

	// Acceleration structure
	Microsoft::WRL::ComPtr<ID3D12Resource> bottomLevelAccelerationStructure;
	Microsoft::WRL::ComPtr<ID3D12Resource> topLevelAccelerationStructure;
	Microsoft::WRL::ComPtr<ID3D12Resource> accelerationScratchResource;
	Microsoft::WRL::ComPtr<ID3D12Resource> instanceDescs;
	D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO topLevelPrebuildInfo = {};
	D3D12_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO bottomLevelPrebuildInfo = {};
	UINT bottomLevelASDescriptorHeapIndex = UINT_MAX;
	UINT topLevelASDescriptorHeapIndex = UINT_MAX;
	WRAPPED_GPU_POINTER fallbackTopLevelAccelerationStructurePointer;
	D3D12_RAYTRACING_GEOMETRY_DESC blGeometryDesc;
	D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS ASBuildFlags;

	//Raytracing output
	Microsoft::WRL::ComPtr<ID3D12Resource> raytracingOutput;
	D3D12_GPU_DESCRIPTOR_HANDLE raytracingOutputResourceUAVGpuDescriptor; //Handle to the output resource Unordered access view
	UINT raytracingOutputResourceUAVDescriptorHeapIndex = UINT_MAX;
};

