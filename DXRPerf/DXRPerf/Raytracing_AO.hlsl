//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

#ifndef RAYTRACING_HLSL
#define RAYTRACING_HLSL

#define HLSL
#include "RaytracingHlslCompat.h"
#include "RaytracingShaderUtil.hlsli"

RaytracingAccelerationStructure Scene : register(t0, space0);
RWTexture2D<float4> RenderTarget : register(u0);
ByteAddressBuffer Indices : register(t1, space0);
StructuredBuffer<Vertex> Vertices : register(t2, space0);

RWBuffer<float4> bounceVertexMap : register(u1);
RWBuffer<float4> bounceNormalMap : register(u2);
RWBuffer<float> AORegister : register(u3);

ConstantBuffer<SceneConstantBuffer> g_sceneCB : register(b0);
ConstantBuffer<ModelConstantBuffer> g_modelCB : register(b1);

typedef BuiltInTriangleIntersectionAttributes TriangleAttributes;

static float3 HemispherePoints[16] =
{
	float3(0.61, 0.61, 0.50),
	float3(0.00, 0.87, 0.50),
	float3(-0.61, 0.61, 0.50),
	float3(-0.87, 0.00, 0.50),
	float3(-0.61,-0.61, 0.50),
	float3(0.00,-0.87, 0.50),
	float3(0.61,-0.61, 0.50),
	float3(0.87, 0.00, 0.50),
	float3(0.35, 0.35, 0.87),
	float3(0.00, 0.50, 0.87),
	float3(-0.35, 0.35, 0.87),
	float3(-0.50, 0.00, 0.87),
	float3(-0.35,-0.35, 0.87),
	float3(0.00,-0.50, 0.87),
	float3(0.35,-0.35, 0.87),
	float3(0.50, 0.00, 0.87)
};


float3 RotateHemisphereToNormal(float3 p, float3 normal)
{
	//Construct local coordinate system
	float3 W = float3(1.f, 0.0f, 0.0f);

	if (abs(normal.x) > 0.99f)
	{
		W.x = 0.f;
		W.y = 1.f;
	}

	float3 T = normalize(cross(normal, W));
	float3 B = cross(T, normal);

	float3 rotatedP = p.x * T + p.y * B + p.z * normal;

	return rotatedP;
}

// Trace a shadow ray and return true if it hits any geometry.
bool TraceShadowRayAndReportIfHit(in Ray ray, in UINT currentRayRecursionDepth)
{
	// Set the ray's extents.
	RayDesc rayDesc;
	rayDesc.Origin = ray.origin;
	rayDesc.Direction = ray.direction;
	// Set TMin to a zero value to avoid aliasing artifcats along contact areas.
	// Note: make sure to enable back-face culling so as to avoid surface face fighting.
	rayDesc.TMin = 0.001;
	rayDesc.TMax = 10000;	// ToDo set this to dist to light

							// Initialize shadow ray payload.
							// Set the initial value to true since closest and any hit shaders are skipped. 
							// Shadow miss shader, if called, will set it to false.
	ShadowRayPayload shadowPayload = { true };
	TraceRay(Scene,
		RAY_FLAG_CULL_BACK_FACING_TRIANGLES
		| RAY_FLAG_ACCEPT_FIRST_HIT_AND_END_SEARCH
		| RAY_FLAG_FORCE_OPAQUE             // ~skip any hit shaders
		| RAY_FLAG_SKIP_CLOSEST_HIT_SHADER, // ~skip closest hit shaders,
		TraceRayParameters::InstanceMask,
		TraceRayParameters::HitGroup::Offset[RayType::Shadow],
		TraceRayParameters::HitGroup::GeometryStride,
		TraceRayParameters::MissShader::Offset[RayType::Shadow],
		rayDesc, shadowPayload);

	return shadowPayload.hit;
}

[shader("raygeneration")]
void MyRaygenShader()
{
	//Generate a ray from the bounce location
	uint2 rayIndex = DispatchRaysIndex().xy;
	uint mapIndex = rayIndex.y * RESWIDTH + rayIndex.x;

	//Check if there is a bounce
	if (bounceVertexMap[mapIndex].w > 0.f)
	{
		float3 hitPosition = bounceVertexMap[mapIndex].xyz;
		float3 hitNormal = bounceNormalMap[mapIndex].xyz;

		float4 finalColor = float4(0.f, 0.f, 0.f, 0.f);

		//Using the unused w variable in the bounce normal map here for index tracking between frames
		unsigned int hPointIndex = (unsigned int)bounceNormalMap[mapIndex].w;
		bounceNormalMap[mapIndex].w++;
		float3 direction = RotateHemisphereToNormal(HemispherePoints[hPointIndex], hitNormal);

		RayDesc rayDesc;
		rayDesc.Origin = hitPosition;
		rayDesc.Direction = direction;
		rayDesc.TMin = 0.001;
		rayDesc.TMax = 10000;

		RayPayload rayPayload = { float4(0, 0, 0, 0), 0 };
		TraceRay(Scene,
			RAY_FLAG_CULL_BACK_FACING_TRIANGLES,
			TraceRayParameters::InstanceMask,
			TraceRayParameters::HitGroup::Offset[RayType::Radiance],
			TraceRayParameters::HitGroup::GeometryStride,
			TraceRayParameters::MissShader::Offset[RayType::Radiance],
			rayDesc, rayPayload);

		AORegister[mapIndex] += rayPayload.color.x;

		finalColor += rayPayload.color;
		
		// Write the raytraced color to the output texture.
		RenderTarget[DispatchRaysIndex().xy] = finalColor;

	}
	else
	{
		//Return black
		RenderTarget[DispatchRaysIndex().xy] = float4(0.f, 0.f, 0.f, 0.f);
	}
}

[shader("closesthit")]
void MyClosestHitShader(inout RayPayload payload, in TriangleAttributes attr)
{
	payload.color = float4(1.f, 1.f, 1.f, 1.f);
}

[shader("miss")]
void MyMissShader(inout RayPayload payload)
{
	float4 background = float4(0.f, 0.f, 0.f, 0.f);
	payload.color = background;
}

[shader("miss")]
void MyMissShader_ShadowRay(inout ShadowRayPayload rayPayload)
{
	rayPayload.hit = false;
}

#endif // RAYTRACING_HLSL