//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

#ifndef RAYTRACING_HLSL
#define RAYTRACING_HLSL

#define HLSL
#include "RaytracingHlslCompat.h"
#include "RaytracingShaderUtil.hlsli"

RaytracingAccelerationStructure Scene : register(t0, space0);
RWTexture2D<float4> RenderTarget : register(u0);
ByteAddressBuffer Indices : register(t1, space0);
StructuredBuffer<Vertex> Vertices : register(t2, space0);

RWBuffer<float4> bounceVertexMap : register(u1);
RWBuffer<float4> bounceNormalMap : register(u2);

ConstantBuffer<SceneConstantBuffer> g_sceneCB : register(b0);
ConstantBuffer<ModelConstantBuffer> g_modelCB : register(b1);

typedef BuiltInTriangleIntersectionAttributes TriangleAttributes;

// Diffuse lighting calculation.
float CalculateDiffuseCoefficient(in float3 hitPosition, in float3 incidentLightRay, in float3 normal)
{
	float fNDotL = saturate(dot(-incidentLightRay, normal));
	return fNDotL;
}

// Phong lighting specular component
float4 CalculateSpecularCoefficient(in float3 hitPosition, in float3 incidentLightRay, in float3 normal, in float specularPower)
{
	float3 reflectedLightRay = normalize(reflect(incidentLightRay, normal));
	return pow(saturate(dot(reflectedLightRay, normalize(-WorldRayDirection()))), specularPower);
}

// Phong lighting model = ambient + diffuse + specular components.
float4 CalculatePhongLighting(in float4 albedo, in float3 normal, in bool isInShadow, in float diffuseCoef = 1.0, in float specularCoef = 1.0, in float specularPower = 50)
{
	float3 hitPosition = HitWorldPosition();
	float3 lightPosition = g_sceneCB.lightPosition.xyz;
	float shadowFactor = isInShadow ? InShadowRadiance : 1.0;
	float3 incidentLightRay = normalize(hitPosition - lightPosition);

	// Diffuse component.
	float4 lightDiffuseColor = g_sceneCB.lightDiffuseColor;
	float Kd = CalculateDiffuseCoefficient(hitPosition, incidentLightRay, normal);
	float4 diffuseColor = shadowFactor * diffuseCoef * Kd * lightDiffuseColor * albedo;

	// Specular component.
	float4 specularColor = float4(0, 0, 0, 0);
	if (!isInShadow)
	{
		float4 lightSpecularColor = float4(1, 1, 1, 1);
		float4 Ks = CalculateSpecularCoefficient(hitPosition, incidentLightRay, normal, specularPower);
		specularColor = specularCoef * Ks * lightSpecularColor;
	}

	// Ambient component.
	float4 ambientColor = g_sceneCB.lightAmbientColor * albedo;

	return ambientColor + diffuseColor + specularColor;
}

//Trace a radiance ray into the scene and return a shaded color
float4 TraceRadianceRay(in Ray ray, in UINT currentRayRecursionDepth)
{
	if (currentRayRecursionDepth >= MAX_RAY_RECURSION_DEPTH)
	{
		return float4(0, 0, 0, 0);
	}

	//Set the ray's extents.
	RayDesc rayDesc;
	rayDesc.Origin = ray.origin;
	rayDesc.Direction = ray.direction;

	rayDesc.TMin = 0.001;
	rayDesc.TMax = 10000;

	RayPayload rayPayload = { float4(0, 0, 0, 0), currentRayRecursionDepth + 1 };
	TraceRay(Scene,
		RAY_FLAG_CULL_BACK_FACING_TRIANGLES,
		TraceRayParameters::InstanceMask,
		TraceRayParameters::HitGroup::Offset[RayType::Radiance],
		TraceRayParameters::HitGroup::GeometryStride,
		TraceRayParameters::MissShader::Offset[RayType::Radiance],
		rayDesc, rayPayload);

	return rayPayload.color;
}

[shader("raygeneration")]
void MyRaygenShader()
{
	//Generate a ray from the bounce location
	uint2 rayIndex = DispatchRaysIndex().xy;
	uint mapIndex = rayIndex.y * RESWIDTH + rayIndex.x;

	//Check if the bounce actually hit anything
	if (bounceVertexMap[mapIndex].w > 0.f)
	{
		//Reflection
		float4 reflectedColor = float4(0, 0, 0, 0);
		float reflectanceCoef = 1.0f;
		float diffuseCoef = 0.9f;
		float specularCoef = 0.7f;
		float specularPower = 50.0f;

		Ray cameraRay = GenerateCameraRay(rayIndex, g_sceneCB.cameraPosition.xyz, g_sceneCB.projectionToWorld);

		if (reflectanceCoef > 0.001)
		{
			//Trace a reflection ray
			Ray reflectionRay = { bounceVertexMap[mapIndex].xyz, reflect(cameraRay.direction, bounceNormalMap[mapIndex].xyz) };
			float4 reflectionColor = TraceRadianceRay(reflectionRay, 0);

			float3 fresnelR = FresnelReflectanceSchlick(cameraRay.direction, bounceNormalMap[mapIndex].xyz, Albedo.xyz);
			reflectedColor = reflectanceCoef * float4(fresnelR, 1) * reflectionColor;
		}

		// Calculate final color.
		float4 phongColor = CalculatePhongLighting(Albedo, bounceNormalMap[mapIndex].xyz, false, diffuseCoef, specularCoef, specularPower);
		float4 color = (phongColor + reflectedColor);

		// Apply visibility falloff.
		float t = RayTCurrent();
		color = lerp(color, BackgroundColor, 1.0 - exp(-0.000002*t*t*t));

		// Write the raytraced color to the output texture.
		RenderTarget[DispatchRaysIndex().xy] = color;
	}
	else
	{
		//Return black
		RenderTarget[DispatchRaysIndex().xy] = float4(0.f, 0.f, 0.f, 0.f);
	}
}


[shader("closesthit")]
void MyClosestHitShader(inout RayPayload payload, in TriangleAttributes attr)
{
	payload.color = float4(1.f, 1.f, 1.f, 1.f);
}

[shader("miss")]
void MyMissShader(inout RayPayload payload)
{
	float4 background = float4(BackgroundColor);
	payload.color = background;
}

[shader("miss")]
void MyMissShader_ShadowRay(inout ShadowRayPayload rayPayload)
{
	rayPayload.hit = false;
}

#endif // RAYTRACING_HLSL