#pragma once

class PerfWriter
{
public:
	static PerfWriter& Instance()
	{
		static PerfWriter *instance = new PerfWriter();
		return *instance;
	}

	//Write text to file, forces a flush
	void Write(std::string text)
	{
		if (!fileOutput.bad())
		{
			fileOutput << text;
			fileOutput.flush();
		}
	}

	std::ofstream fileOutput;
private:
	PerfWriter()
	{
#if 0
		//fileOutput = std::ofstream(LogFile, std::ofstream::app); //append log
#else
		fileOutput = std::ofstream(LogFile); //overwrite log
#endif
	}

	std::string LogFile = "perflog.txt";

};