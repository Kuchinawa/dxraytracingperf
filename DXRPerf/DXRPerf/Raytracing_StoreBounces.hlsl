//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

#ifndef RAYTRACING_HLSL
#define RAYTRACING_HLSL

#define HLSL
#include "RaytracingHlslCompat.h"
#include "RaytracingShaderUtil.hlsli"

RaytracingAccelerationStructure Scene : register(t0, space0);
RWTexture2D<float4> RenderTarget : register(u0);
ByteAddressBuffer Indices : register(t1, space0);
StructuredBuffer<Vertex> Vertices : register(t2, space0);

RWBuffer<float4> bounceVertexMap : register(u1);
RWBuffer<float4> bounceNormalMap : register(u2);

ConstantBuffer<SceneConstantBuffer> g_sceneCB : register(b0);
ConstantBuffer<ModelConstantBuffer> g_modelCB : register(b1);

typedef BuiltInTriangleIntersectionAttributes TriangleAttributes;

[shader("raygeneration")]
void MyRaygenShader()
{
	//Generate a ray through a camera pixel
	Ray ray = GenerateCameraRay(DispatchRaysIndex().xy, g_sceneCB.cameraPosition.xyz, g_sceneCB.projectionToWorld);

	//Set the ray's extents.
	RayDesc rayDesc;
	rayDesc.Origin = ray.origin;
	rayDesc.Direction = ray.direction;

	rayDesc.TMin = 0.001;
	rayDesc.TMax = 10000;

	RayPayload rayPayload = { float4(0, 0, 0, 0), 0 };
	TraceRay(Scene,
		RAY_FLAG_CULL_BACK_FACING_TRIANGLES,
		TraceRayParameters::InstanceMask,
		TraceRayParameters::HitGroup::Offset[RayType::Radiance],
		TraceRayParameters::HitGroup::GeometryStride,
		TraceRayParameters::MissShader::Offset[RayType::Radiance],
		rayDesc, rayPayload);

	// Write the raytraced color to the output texture.
	RenderTarget[DispatchRaysIndex().xy] = rayPayload.color;
}

[shader("closesthit")]
void MyClosestHitShader(inout RayPayload payload, in TriangleAttributes attr)
{
	// Get the base index of the triangle's first 32 bit index.
	uint indexSizeInBytes = 4;
	uint indicesPerTriangle = 3;
	uint triangleIndexStride = indicesPerTriangle * indexSizeInBytes;
	uint baseIndex = PrimitiveIndex() * triangleIndexStride;

	const uint3 indices = Load3x32BitIndices(baseIndex, Indices);

	// Retrieve corresponding vertex normals for the triangle vertices.
	float3 vertexNormals[3] = {
		Vertices[indices[0]].normal,
		Vertices[indices[1]].normal,
		Vertices[indices[2]].normal
	};

	float3 triangleNormal = HitAttribute(vertexNormals, attr);

	//Trace a shadow ray.
	float3 hitPosition = HitWorldPosition();

	uint2 rayIndex = DispatchRaysIndex().xy;

	uint mapIndex = rayIndex.y * RESWIDTH + rayIndex.x;

	bounceVertexMap[mapIndex] = float4(hitPosition, 1.0f);
	bounceNormalMap[mapIndex] = float4(triangleNormal, 0.0f);

	payload.color = float4(hitPosition, 1.f);
}

[shader("miss")]
void MyMissShader(inout RayPayload payload)
{
	uint2 rayIndex = DispatchRaysIndex().xy;
	uint mapIndex = rayIndex.y * RESWIDTH + rayIndex.x;

	bounceVertexMap[mapIndex] = float4(0.0f, 0.0f, 0.0f, 0.0f);
	bounceNormalMap[mapIndex] = float4(0.0f, 0.0f, 0.0f, 0.0f);

	payload.color = BackgroundColor;
}

[shader("miss")]
void MyMissShader_ShadowRay(inout ShadowRayPayload rayPayload)
{
	rayPayload.hit = false;
}

#endif // RAYTRACING_HLSL