//Precompiled header file
#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers.
#endif

#include <Windows.h>

#include <sstream>
#include <iomanip>

#include <string>
#include <wrl.h>
#include <shellapi.h>
#include <memory>
#include <unordered_map>
#include <vector>
#include <assert.h>

#include <dxgi1_6.h>
#include "d3d12_1.h"
#include <atlbase.h>
#include "D3D12RaytracingFallback.h"
#include "D3D12RaytracingPrototypeHelpers.hpp"
#include "d3dx12.h"
#include "StepTimer.h"

#include <fstream>
#include <sstream>

#include <locale> // wstring -> string
#include <codecvt>// /
#include "PerfWriter.h"

#ifdef _DEBUG
#include <dxgidebug.h>
#endif


#include <DirectXMath.h>
#include "GPUTimer.h"
#include "RaytracingHlslCompat.h"
#include "DXSampleHelper.h"

#include "Model.h"
#include "DXContext.h"
#include "DXRaytracingFallback.h"
#include "SystemContext.h"

