#pragma once

class Model
{
public:
	Model(const std::string modelPath);
	~Model();

	void LoadModel(const std::string modelPath);

	std::vector<Vertex> vertices;
	std::vector<Index> indices;

	UINT triCount = 0;
};

