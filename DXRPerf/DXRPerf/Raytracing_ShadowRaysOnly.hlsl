//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

#ifndef RAYTRACING_HLSL
#define RAYTRACING_HLSL

#define HLSL
#include "RaytracingHlslCompat.h"
#include "RaytracingShaderUtil.hlsli"

RaytracingAccelerationStructure Scene : register(t0, space0);
RWTexture2D<float4> RenderTarget : register(u0);
ByteAddressBuffer Indices : register(t1, space0);
StructuredBuffer<Vertex> Vertices : register(t2, space0);

RWBuffer<float4> bounceVertexMap : register(u1);
RWBuffer<float4> bounceNormalMap : register(u2);

ConstantBuffer<SceneConstantBuffer> g_sceneCB : register(b0);
ConstantBuffer<ModelConstantBuffer> g_modelCB : register(b1);

typedef BuiltInTriangleIntersectionAttributes TriangleAttributes;

// Trace a shadow ray and return true if it hits any geometry.
bool TraceShadowRayAndReportIfHit(in Ray ray, in UINT currentRayRecursionDepth)
{
	// Set the ray's extents.
	RayDesc rayDesc;
	rayDesc.Origin = ray.origin;
	rayDesc.Direction = ray.direction;
	// Set TMin to a zero value to avoid aliasing artifcats along contact areas.
	// Note: make sure to enable back-face culling so as to avoid surface face fighting.
	rayDesc.TMin = 0.001;
	rayDesc.TMax = 10000;	// ToDo set this to dist to light

							// Initialize shadow ray payload.
							// Set the initial value to true since closest and any hit shaders are skipped. 
							// Shadow miss shader, if called, will set it to false.
	ShadowRayPayload shadowPayload = { true };
	TraceRay(Scene,
		RAY_FLAG_CULL_BACK_FACING_TRIANGLES
		| RAY_FLAG_ACCEPT_FIRST_HIT_AND_END_SEARCH
		| RAY_FLAG_FORCE_OPAQUE             // ~skip any hit shaders
		| RAY_FLAG_SKIP_CLOSEST_HIT_SHADER, // ~skip closest hit shaders,
		TraceRayParameters::InstanceMask,
		TraceRayParameters::HitGroup::Offset[RayType::Shadow],
		TraceRayParameters::HitGroup::GeometryStride,
		TraceRayParameters::MissShader::Offset[RayType::Shadow],
		rayDesc, shadowPayload);

	return shadowPayload.hit;
}

[shader("raygeneration")]
void MyRaygenShader()
{
	//Generate a ray from the bounce location
	uint2 rayIndex = DispatchRaysIndex().xy;
	uint mapIndex = rayIndex.y * RESWIDTH + rayIndex.x;

	//Check if the bounce actually hit anything
	if (bounceVertexMap[mapIndex].w > 0.f)
	{
		float3 hitPosition = bounceVertexMap[mapIndex].xyz;

		Ray shadowRay = { hitPosition, normalize(g_sceneCB.lightPosition.xyz - hitPosition) };

		//Cast ray into the scene, and retrieve a shadow bool
		bool color = TraceShadowRayAndReportIfHit(shadowRay, 0.f);

		// Write the raytraced color to the output texture.
		RenderTarget[DispatchRaysIndex().xy] = color;
	}
	else
	{
		//Return black
		RenderTarget[DispatchRaysIndex().xy] = float4(0.f, 0.f, 0.f, 0.f);
	}
}

[shader("closesthit")]
void MyClosestHitShader(inout RayPayload payload, in TriangleAttributes attr)
{
	// Get the base index of the triangle's first 32 bit index.
	uint indexSizeInBytes = 4;
	uint indicesPerTriangle = 3;
	uint triangleIndexStride = indicesPerTriangle * indexSizeInBytes;
	uint baseIndex = PrimitiveIndex() * triangleIndexStride;

	const uint3 indices = Load3x32BitIndices(baseIndex, Indices);

	// Retrieve corresponding vertex normals for the triangle vertices.
	float3 vertexNormals[3] = {
		Vertices[indices[0]].normal,
		Vertices[indices[1]].normal,
		Vertices[indices[2]].normal
	};

	float3 triangleNormal = HitAttribute(vertexNormals, attr);

	//Trace a shadow ray.
	float3 hitPosition = HitWorldPosition();

	uint2 rayIndex = DispatchRaysIndex().xy;

	uint mapIndex = rayIndex.y * RESWIDTH + rayIndex.x;

	bounceVertexMap[mapIndex] = float4(hitPosition, 1.0f);
	bounceNormalMap[mapIndex] = float4(triangleNormal, 1.0f);

	payload.color = float4(hitPosition, 1.f);
}

[shader("miss")]
void MyMissShader(inout RayPayload payload)
{
	float4 background = float4(BackgroundColor);
	payload.color = background;
}

[shader("miss")]
void MyMissShader_ShadowRay(inout ShadowRayPayload rayPayload)
{
	rayPayload.hit = false;
}

#endif // RAYTRACING_HLSL