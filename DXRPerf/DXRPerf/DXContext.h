#pragma once

interface IDeviceNotify;

class DXContext
{
public:
	DXContext();
	~DXContext();

	void WaitForGpu() noexcept;

	void OnDestroy();
	bool OnSizeChanged(UINT width, UINT height, bool minimized);

	void SetTitle(LPCWSTR text);

	void SetWindow(HWND window, int width, int height, std::wstring windowName);
	void InitializeDXGIAdapter();
	void EnableDXRExperimentalFeatures();
	void CreateDevice();
	void CreateWindowSizeDependentResources();

	void ResetCommandAllocatorAndCommandlist();

	void RegisterDeviceNotify(IDeviceNotify* deviceNotify)
	{
		this->deviceNotify = deviceNotify; //This is probably dumb, just ignore and move on..

										   //On RS4 and higher, applications that handle device removal should declare themselves as being able to do so
		__if_exists(DXGIDeclareAdapterRemovalSupport)
		{
			if (deviceNotify)
			{
				if (FAILED(DXGIDeclareAdapterRemovalSupport()))
				{
					OutputDebugString(L"Warning: application failed to declare adapter removal support.\n");
				}
			}
		}
	}

	UINT GetWidth() const { return width; };
	UINT GetHeight() const { return height; };
	float GetAspectRatio() const { return aspectRatio; };
	bool IsMinimized() const { return minimized; };

	std::wstring GetAdapterDescription()  const { return adapterDescription; };
	ID3D12Device* GetDevice() const { return d3dDevice.Get(); };
	ID3D12GraphicsCommandList* GetCommandList() const { return commandList.Get(); };
	ID3D12CommandQueue* GetCommandQueue() const { return commandQueue.Get(); };
	ID3D12CommandAllocator* GetCommandAllocator() const { return commandAllocators[backBufferIndex].Get(); };
	DXGI_FORMAT GetBackBufferFormat() const { return backBufferFormat; }
	ID3D12Resource* GetRenderTarget() const { return renderTargets[backBufferIndex].Get(); }
	UINT GetCurrentFrameIndex() const { return backBufferIndex; }
	UINT GetBackBufferCount() const { return backBufferCount; }
	void ExecuteCommandList();

	void UpdateForSizeChange(UINT newWidth, UINT newHeight);

	//Per frame functions
	void Prepare(D3D12_RESOURCE_STATES beforeState = D3D12_RESOURCE_STATE_PRESENT);
	void Present(D3D12_RESOURCE_STATES beforeState = D3D12_RESOURCE_STATE_RENDER_TARGET);

private:

	void MoveToNextFrame();

	void ResizeWindow(UINT width, UINT height);

	void HandleDeviceLost();


	HWND windowHandle;
	std::wstring windowTitle;
	RECT outputSize;

	bool minimized;

	//Window dimensions
	UINT width;
	UINT height;
	float aspectRatio;

	const static size_t MAX_BACK_BUFFER_COUNT = 3;

	UINT backBufferIndex = 0; //Swap chain buffer index

	Microsoft::WRL::ComPtr<IDXGIAdapter1> adapter;
	UINT adapterID;
	std::wstring adapterDescription;

	//Direct3D objects
	Microsoft::WRL::ComPtr<ID3D12Device> d3dDevice;
	Microsoft::WRL::ComPtr<ID3D12CommandQueue> commandQueue;
	Microsoft::WRL::ComPtr<ID3D12CommandAllocator> commandAllocators[MAX_BACK_BUFFER_COUNT];
	Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> commandList;

	//Swapchain objects
	Microsoft::WRL::ComPtr<IDXGISwapChain3> swapChain;
	Microsoft::WRL::ComPtr<IDXGIFactory4> dxgiFactory;
	Microsoft::WRL::ComPtr<ID3D12Resource> renderTargets[MAX_BACK_BUFFER_COUNT];
	Microsoft::WRL::ComPtr<ID3D12Resource> depthStencil;

	//Presentation fence objects
	Microsoft::WRL::ComPtr<ID3D12Fence> fence;
	UINT64 fenceValues[MAX_BACK_BUFFER_COUNT];
	Microsoft::WRL::Wrappers::Event fenceEvent;

	//Direct3D rendering objects
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> rtvDescriptorHeap; //Render target view
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> dsvDescriptorHeap; //Depth stencil view
	UINT rtvDescriptorSize;
	D3D12_VIEWPORT screenViewport;
	D3D12_RECT scissorRect;

	//Direct3D properties
	DXGI_FORMAT depthBufferFormat = DXGI_FORMAT_UNKNOWN;
	DXGI_FORMAT backBufferFormat = DXGI_FORMAT_R8G8B8A8_UNORM;
	UINT backBufferCount = 3; //FrameCount
	D3D_FEATURE_LEVEL d3dFeatureLevel;
	D3D_FEATURE_LEVEL minFeatureLevel = D3D_FEATURE_LEVEL_11_0;

	bool VSYNC = false;
	bool isFullscreen = false;
	UINT forceAdapter = UINT_MAX;

	// The IDeviceNotify can be held directly as it owns the DeviceResources.
	IDeviceNotify* deviceNotify;

};