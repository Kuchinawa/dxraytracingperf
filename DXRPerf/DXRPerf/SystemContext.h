#pragma once

class SystemContext
{
public:
	SystemContext(UINT width, UINT height, std::wstring windowName);
	~SystemContext();

	void SetTitle(LPCWSTR title);

	int Run(HINSTANCE hInstance, int nCmdShow, WNDPROC wndProc);


	void ParseCommandLineArgs(_In_reads_(argc) WCHAR* argv[], int argc);

	//Accepts the windows system messages and handles them. eg keyboard presses.
	LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM);

private:

	void OnKeyDown(UINT key);
	void OnKeyUp(UINT key);

	//Window handle
	HWND hwnd;

	//Window settings
	std::wstring windowTitle;
	bool fullScreen;
	RECT windowRect;
	const UINT windowStyle = WS_OVERLAPPEDWINDOW;

	void RunTest(std::shared_ptr<Model> model, float testDurationInSec, std::string modelName, ASBuildType buildTypeAS, bool rebuildAS, bool updateAS);

	//Bounds
	RECT windowBounds;

	//DXR
	std::unique_ptr<DXRaytracingFallback> fallbackContext;
};

