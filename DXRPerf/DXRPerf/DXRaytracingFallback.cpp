#include "precomp.h"
#include "DXRaytracingFallback.h"
#include "DirectXRaytracingHelper.h"
#include "CompiledShaders\Raytracing.hlsl.h"
#include "CompiledShaders\Raytracing_Diffuse.hlsl.h"
#include "CompiledShaders\Raytracing_PrimaryRaysOnly.hlsl.h"
#include "CompiledShaders\Raytracing_ShadowRaysOnly.hlsl.h"
#include "CompiledShaders\Raytracing_StoreBounces.hlsl.h"
#include "CompiledShaders\Raytracing_AO.hlsl.h"


using namespace std;
using Microsoft::WRL::ComPtr;

const wchar_t* DXRaytracingFallback::hitGroupName = L"MyHitGroup";
const wchar_t* DXRaytracingFallback::raygenShaderName = L"MyRaygenShader";
const wchar_t* DXRaytracingFallback::closestHitShaderName = L"MyClosestHitShader";
const wchar_t* DXRaytracingFallback::missShaderName = L"MyMissShader";

//Compiled shaders, (Avoid hovering over the g_p* variables in VS, it will hang...)
CD3DX12_SHADER_BYTECODE shaders[6] =
{
	CD3DX12_SHADER_BYTECODE((void *)g_pRaytracing_StoreBounces, ARRAYSIZE(g_pRaytracing_StoreBounces)),
	CD3DX12_SHADER_BYTECODE((void *)g_pRaytracing_AO, ARRAYSIZE(g_pRaytracing_AO)),
	CD3DX12_SHADER_BYTECODE((void *)g_pRaytracing_PrimaryRaysOnly, ARRAYSIZE(g_pRaytracing_PrimaryRaysOnly)),
	CD3DX12_SHADER_BYTECODE((void *)g_pRaytracing_ShadowRaysOnly, ARRAYSIZE(g_pRaytracing_ShadowRaysOnly)),
	CD3DX12_SHADER_BYTECODE((void *)g_pRaytracing_Diffuse, ARRAYSIZE(g_pRaytracing_Diffuse)),
	CD3DX12_SHADER_BYTECODE((void *)g_pRaytracing, ARRAYSIZE(g_pRaytracing))
};

const string shaderNames[6] =
{
	"Store bounces",
	"Calculate AO",
	"Primary rays",
	"Shadow rays",
	"Diffuse rays",
	"Full ray trace"
};

//Lazy support for wstring..
const wstring wShaderNames[6] =
{
	L"Store bounces",
	L"Calculate AO",
	L"Primary rays",
	L"Shadow rays",
	L"Diffuse rays",
	L"Full ray trace"
};

UINT maxRecursionDepthPerShader[6] =
{
	1,
	1,
	1,
	1,
	1,
	3
};


DXRaytracingFallback::DXRaytracingFallback(float testDurationInSec, std::shared_ptr<Model> model, bool ASRebuild, ASBuildType asBuildType, bool ASAllowUpdate) :
	testDurationInSec(testDurationInSec),
	sceneModel(model), //OBJ model
	ASRebuild(ASRebuild), //Force rebuild every frame?
	asBuildType(asBuildType),
	ASAllowUpdate(ASAllowUpdate)
{
	UpdateForSizeChange(RESWIDTH, RESHEIGHT);
}


DXRaytracingFallback::~DXRaytracingFallback()
{
}

void DXRaytracingFallback::HandleInput(UINT key)
{

	switch (key)
	{
	case 49:  //1
		ASRebuild = !ASRebuild;
		break;
	case 50: //2
	{
		switch (asBuildType)
		{
		case FastBuild:
			asBuildType = FastTrace;
			ASReinit = true;
			break;
		case FastTrace:
			asBuildType = FastBuild;
			ASReinit = true;
			break;
		default:
			break;
		}
	}

	break;

	case 87: //w
	{

		//XMVECTOR forward = XMVector4Normalize(cameraLookAt - cameraEye);
		XMVECTOR forward = { 0,0,1,0 };
		cameraEye += forward;
		cameraLookAt += forward;
		UpdateCameraMatrices();
	}
	break;
	case 83: //s
	{
		//XMVECTOR backward = XMVector4Normalize(cameraLookAt - cameraEye);
		XMVECTOR backward = { 0,0,-1,0 };
		cameraEye += backward;
		cameraLookAt += backward;
		UpdateCameraMatrices();
	}
	break;
	case 65: //a
	{
		XMVECTOR left = { -1,0,0,0 };
		cameraEye += left;
		cameraLookAt += left;
		UpdateCameraMatrices();
	}
	break;
	case 68: //d
	{
		XMVECTOR right = { 1,0,0,0 };
		cameraEye += right;
		cameraLookAt += right;
		UpdateCameraMatrices();
	}
	break;
	case 37: //Left
	{
		//Rotate camera around Y axis
		XMMATRIX rotate = XMMatrixRotationY(XMConvertToRadians(-10.f));
		cameraEye = XMVector3Transform(cameraEye, rotate);
		cameraUp = XMVector3Transform(cameraUp, rotate);
		cameraLookAt = XMVector3Transform(cameraLookAt, rotate);
		UpdateCameraMatrices();
	}
	break;
	case 39: //Right
	{
		//Rotate camera around Y axis
		XMMATRIX rotate = XMMatrixRotationY(XMConvertToRadians(10.f));
		cameraEye = XMVector3Transform(cameraEye, rotate);
		cameraUp = XMVector3Transform(cameraUp, rotate);
		cameraLookAt = XMVector3Transform(cameraLookAt, rotate);
		UpdateCameraMatrices();
	}
	break;
	case 38: //Up
	{
		//Rotate camera around X axis
		XMMATRIX rotate = XMMatrixRotationX(XMConvertToRadians(-10.f));
		cameraEye = XMVector3Transform(cameraEye, rotate);
		cameraUp = XMVector3Transform(cameraUp, rotate);
		cameraLookAt = XMVector3Transform(cameraLookAt, rotate);
		UpdateCameraMatrices();
	}
	break;
	case 40: //Down
	{
		//Rotate camera around X axis
		XMMATRIX rotate = XMMatrixRotationX(XMConvertToRadians(10.f));
		cameraEye = XMVector3Transform(cameraEye, rotate);
		cameraUp = XMVector3Transform(cameraUp, rotate);
		cameraLookAt = XMVector3Transform(cameraLookAt, rotate);
		UpdateCameraMatrices();
	}
	break;
	case 17: //Ctrl
	{
		XMVECTOR down = { 0,-1,0,0 };
		cameraEye += down;
		cameraLookAt += down;
		UpdateCameraMatrices();
	}
	break;
	case 32: //Space
	{
		XMVECTOR up = { 0,1,0,0 };
		cameraEye += up;
		cameraLookAt += up;
		UpdateCameraMatrices();
	}
	break;
	//default:
	//	break;
	}

}
// Initialize scene rendering parameters
void DXRaytracingFallback::InitializeScene()
{
	UINT frameIndex = dxContext.GetCurrentFrameIndex();

	//Setup materials
	{
		modelCB.albedo = Albedo;
	}

	//Setup Camera
	{
		//X == Left/Right
		//Y == Up/Down
		//Z == Forward/Backwards
		// + / - respectively

		//Initialize the view and projection inverse matrices
		cameraEye = { 0.f, 5.f, -20.f, 1.0f };
		cameraLookAt = { 0.f, 5.f, -19.f, 1.0f }; //Forward

		XMVECTOR right = { 1.0f, 0.0f, 0.0f, 0.0f };

		XMVECTOR direction = XMVector4Normalize(cameraLookAt - cameraEye);
		cameraUp = XMVector3Normalize(XMVector3Cross(direction, right));

		//Rotate camera around Y axis
		//XMMATRIX rotate = XMMatrixRotationY(XMConvertToRadians(45.0f));
		//cameraEye = XMVector3Transform(cameraEye, rotate);
		//cameraUp = XMVector3Transform(cameraUp, rotate);

		UpdateCameraMatrices();
	}

	//Setup lights
	{
		// Initialize the lighting parameters.
		XMFLOAT4 lightPosition;
		XMFLOAT4 lightAmbientColor;
		XMFLOAT4 lightDiffuseColor;

		lightPosition = XMFLOAT4(0.0f, 0.0f, -1.0f, 0.0f); //Behind camera
		sceneCB.lightPosition = XMLoadFloat4(&lightPosition);

		lightAmbientColor = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
		sceneCB.lightAmbientColor = XMLoadFloat4(&lightAmbientColor);

		lightDiffuseColor = XMFLOAT4(0.5f, 0.0f, 0.0f, 1.0f);
		sceneCB.lightDiffuseColor = XMLoadFloat4(&lightDiffuseColor);

	}

	//Apply the initial values to all frame buffer instances
	//for (auto& sCB : sceneCB)
	//{
	//	sCB = sceneCB[frameIndex];
	//}
}

void DXRaytracingFallback::Update()
{
	timer.Tick();

	CalculatePerformance();

	if (ASReinit)
	{
		dxContext.WaitForGpu();

		dxContext.ResetCommandAllocatorAndCommandlist();


		BuildAccelerationStructures();
		ASReinit = false;

		dxContext.ExecuteCommandList();
		dxContext.WaitForGpu();
	}
}

// Update camera matrices passed into the shader
void DXRaytracingFallback::UpdateCameraMatrices()
{

	sceneCB.cameraPosition = cameraEye;
	float fovAngleY = 45.0f;
	XMMATRIX view = XMMatrixLookAtLH(cameraEye, cameraLookAt, cameraUp);
	XMMATRIX proj = XMMatrixPerspectiveFovLH(XMConvertToRadians(fovAngleY), dxContext.GetAspectRatio(), 1.0f, 125.0f);
	XMMATRIX viewProj = view * proj;

	sceneCB.projectionToWorld = XMMatrixInverse(nullptr, viewProj);
}


void DXRaytracingFallback::Initialize(HWND window, int width, int height, std::wstring windowName)
{

	dxContext.RegisterDeviceNotify(this);
	dxContext.SetWindow(window, width, height, windowName);
	dxContext.InitializeDXGIAdapter();
	dxContext.EnableDXRExperimentalFeatures();
	dxContext.CreateDevice();
	dxContext.CreateWindowSizeDependentResources();

	InitializeScene();

	missShaderTableResource.resize(shaderCount);
	hitGroupShaderTableResource.resize(shaderCount);
	rayGenShaderTableResource.resize(shaderCount);

	CreateDeviceDependentResourcesForRT();
	CreateWindowSizeDependentResourcesForRT();
}

void DXRaytracingFallback::Render()
{
	if (dxContext.IsMinimized())
	{
		return;
	}

	auto commandList = dxContext.GetCommandList();

	dxContext.Prepare(); //Prepare render target

	ASRebuildTimer.BeginFrame(commandList);

	//Rebuild AS
	if (ASRebuild)
	{

		ASRebuildTimer.Start(commandList);

		UpdateAccelerationStructures(true, ASAllowUpdate);

		ASRebuildTimer.Stop(commandList);
	}

	DoRaytracing(); //Trace rays
	CopyRaytracingOutputToBackbuffer(); //Copy result to the render target

	if (currentShaderIndex == 1 && OAframeCount < 16)
	{
		ReadbackAOData();
		OAframeCount++;
	}

	ASRebuildTimer.EndFrame(commandList);

	dxContext.Present(D3D12_RESOURCE_STATE_PRESENT);

	if (currentShaderIndex == 0)
	{
		bouncesStored = true;
	}
}

void DXRaytracingFallback::OnDestroy()
{
	dxContext.WaitForGpu();
	dxContext.OnDestroy();
	OnDeviceLost();
}

void DXRaytracingFallback::OnSizeChanged(UINT width, UINT height, bool minimized)
{
	if (!dxContext.OnSizeChanged(width, height, minimized))
	{
		return;
	}

	UpdateForSizeChange(width, height);

	ReleaseWindowSizeDependentResourcesForRT();
	CreateWindowSizeDependentResourcesForRT();
}


void DXRaytracingFallback::CalculatePerformance()
{


	switch (currentShaderIndex)
	{
	case 0: //Store bounces shader
	{
		//First frame of bounce register shader?
		if (bouncesStored)
		{
			currentShaderIndex++;
		}

		return;
	}
	break;
	case 1: //AO shader
	{
		if (OAframeCount == 16)
		{
			//Map the readback pointer
			void* pMappedData;
			ThrowIfFailed(AOReadbackBuffer->Map(0, nullptr, &pMappedData));

			//Copy to local array to prevent weird stuff...
			UINT numElements = dxContext.GetWidth() * dxContext.GetHeight();
			vector<float> AOList(numElements, 0.f);
			memcpy(AOList.data(), pMappedData, numElements * sizeof(float));

			AOReadbackBuffer->Unmap(0, nullptr);


			float sum = 0.f;
			for (UINT i = 0; i < numElements; i++)
			{
				sum += AOList[i];
			}
			sum /= numElements;
			sum /= 16; // 0/16..16/16


			std::stringstream logString;
			logString << "    AO: " << sum << "\n";
			PerfWriter::Instance().Write(logString.str());

			currentShaderIndex++;
		}
		return;
	}
	break;
	default:
		break;
	}

	static unsigned int frameCount = 0;
	static double elapsedTime = 0.0f; //(Current time really..) //TODO: non static class member?
	static double elapsedTestTime = 0.0f;
	double totalTime = timer.GetTotalSeconds();
	frameCount++;

	//Compute average over one second
	if ((totalTime - elapsedTime) >= 1.0)
	{
		double diff = static_cast<double>(totalTime - elapsedTime);
		double fps = static_cast<double>(frameCount) / diff; //Normalize to an exact second

		frameCount = 0;
		elapsedTime = totalTime;

		double primaryRaysPerSecond = (dxContext.GetWidth()* dxContext.GetHeight() * fps) / static_cast<double>(1e6);

		wstringstream windowText;

		windowText << setprecision(2) << fixed
			<< L"    fps: " << fps
			<< L"    Million primary rays/s: " << primaryRaysPerSecond
			<< L"    GPU: " << dxContext.GetAdapterDescription().c_str()
			<< L"    Tris: " << sceneModel->triCount
			<< L"    AS Build type: ";
		switch (asBuildType)
		{
		case FastBuild:
			windowText << "Fast build";
			break;
		case FastTrace:
			windowText << "Fast trace";
			break;
		}

		if (ASRebuild)
		{
			windowText << L"    AS rebuild time: " << ASRebuildTimer.GetElapsedMS() << L"ms";
		}

		windowText << "    Current Shader: " << wShaderNames[currentShaderIndex];

		dxContext.SetTitle(windowText.str().c_str());


		//Log after 10seconds of testing
		if ((totalTime - elapsedTestTime) >= testDurationInSec)
		{
			using convert_type = std::codecvt_utf8<wchar_t>;
			std::wstring_convert<convert_type, wchar_t> converter;

			std::string converted_str = converter.to_bytes(dxContext.GetAdapterDescription());


			std::stringstream logString;
			logString << "Shader: " << shaderNames[currentShaderIndex] << ":\n"
				<< "    GPU: " << converted_str << "\n"
				<< "    fps: " << fps << "\n"
				<< "    Million rays/s: " << primaryRaysPerSecond << "\n"
				<< "    Tris: " << sceneModel->triCount << "\n";

			if (ASRebuild)
			{
				logString << setprecision(2) << "    AS rebuild time: " << ASRebuildTimer.GetElapsedMS() << "ms\n\n";
			}

			PerfWriter::Instance().Write(logString.str());


			if (currentShaderIndex == shaderCount - 1)
			{
				testDone = true;

				elapsedTime = 0.f;
				elapsedTestTime = 0.f;
			}
			else
			{
				currentShaderIndex++;
				elapsedTestTime = totalTime;
			}

		}
	}
}

void DXRaytracingFallback::DoRaytracing()
{
	auto commandList = dxContext.GetCommandList();
	auto frameIndex = dxContext.GetCurrentFrameIndex();


	commandList->SetComputeRootSignature(raytracingGlobalRootSignature.Get());

	//Copy the updated scene constant buffer to GPU.
	memcpy(&mappedConstantData[frameIndex].constants, &sceneCB, sizeof(sceneCB));
	auto cbGPUAddress = perFrameConstants->GetGPUVirtualAddress() + frameIndex * sizeof(mappedConstantData[0]);
	commandList->SetComputeRootConstantBufferView(GlobalRootSignatureParams::SceneConstantSlot, cbGPUAddress);

	//Bind the heaps, acceleration structure and dispatch rays (using the fallback layer)
	D3D12_FALLBACK_DISPATCH_RAYS_DESC dispatchDesc = {};
	fallbackCommandList->SetDescriptorHeaps(1, descriptorHeap.GetAddressOf());
	//Set index and successive vertex buffer descriptor tables
	commandList->SetComputeRootDescriptorTable(GlobalRootSignatureParams::VertexBuffersSlot, indexBuffer.gpuDescriptorHandle);
	commandList->SetComputeRootDescriptorTable(GlobalRootSignatureParams::OutputViewSlot, raytracingOutputResourceUAVGpuDescriptor);
	fallbackCommandList->SetTopLevelAccelerationStructure(GlobalRootSignatureParams::AccelerationStructureSlot, fallbackTopLevelAccelerationStructurePointer);
	commandList->SetComputeRootDescriptorTable(GlobalRootSignatureParams::BounceMapSlot, bounceMapVertexUAVGpuDescriptor);

	//Dispatch rays

	//Since each shader table has only one shader record, the stride is the same as the size.
	//TODO: Update for more
	dispatchDesc.HitGroupTable.StartAddress = hitGroupShaderTableResource[currentShaderIndex]->GetGPUVirtualAddress();
	dispatchDesc.HitGroupTable.SizeInBytes = hitGroupShaderTableResource[currentShaderIndex]->GetDesc().Width;
	dispatchDesc.HitGroupTable.StrideInBytes = dispatchDesc.HitGroupTable.SizeInBytes; //TODO

	dispatchDesc.MissShaderTable.StartAddress = missShaderTableResource[currentShaderIndex]->GetGPUVirtualAddress();
	dispatchDesc.MissShaderTable.SizeInBytes = missShaderTableResource[currentShaderIndex]->GetDesc().Width;
	dispatchDesc.MissShaderTable.StrideInBytes = dispatchDesc.MissShaderTable.SizeInBytes;

	dispatchDesc.RayGenerationShaderRecord.StartAddress = rayGenShaderTableResource[currentShaderIndex]->GetGPUVirtualAddress();
	dispatchDesc.RayGenerationShaderRecord.SizeInBytes = rayGenShaderTableResource[currentShaderIndex]->GetDesc().Width;

	dispatchDesc.Width = dxContext.GetWidth();
	dispatchDesc.Height = dxContext.GetHeight();

	fallbackCommandList->DispatchRays(fallbackStateObject[currentShaderIndex].Get(), &dispatchDesc);
}

//Copy the raytracing output to the backbuffer
void DXRaytracingFallback::CopyRaytracingOutputToBackbuffer()
{
	auto commandList = dxContext.GetCommandList();
	auto renderTarget = dxContext.GetRenderTarget();

	//Switch states to Copy Dest and Copy Source for copying
	D3D12_RESOURCE_BARRIER preCopyBarriers[2];
	preCopyBarriers[0] = CD3DX12_RESOURCE_BARRIER::Transition(renderTarget, D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_COPY_DEST);
	preCopyBarriers[1] = CD3DX12_RESOURCE_BARRIER::Transition(raytracingOutput.Get(), D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_COPY_SOURCE);
	commandList->ResourceBarrier(ARRAYSIZE(preCopyBarriers), preCopyBarriers);

	//Copy raytracing output to the rander target view
	commandList->CopyResource(renderTarget, raytracingOutput.Get());

	//Set the raytracing output back to unordered access, and the render target to the present state
	D3D12_RESOURCE_BARRIER postCopyBarriers[2];
	postCopyBarriers[0] = CD3DX12_RESOURCE_BARRIER::Transition(renderTarget, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PRESENT);
	postCopyBarriers[1] = CD3DX12_RESOURCE_BARRIER::Transition(raytracingOutput.Get(), D3D12_RESOURCE_STATE_COPY_SOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);
	commandList->ResourceBarrier(ARRAYSIZE(postCopyBarriers), postCopyBarriers);
}

//Create resources that depend on the device
void DXRaytracingFallback::CreateDeviceDependentResourcesForRT()
{

	CreateAuxilaryDeviceResources();

	//Initiate the raytracing pipeline

	//Create raytracing interfaces: raytracing device and commandlist
	CreateRaytracingInterfaces();

	//Create root signatures for the shaders
	CreateRootSignatures();

	//Create a raytracing pipeline state object which defines the binding of shaders, state, and resources to be used during raytracing
	for (UINT i = 0; i < shaderCount; i++)
	{
		fallbackStateObject.push_back(CreateRaytracingPipelineStateObject(shaders[i], i));
	}

	//Create descriptor Heap
	CreateDescriptorHeap();

	//Build geometry
	BuildGeometry();

	//Initialize Test Buffers
	InitializeBumpMap();

	InitializeAO();

	//Build raytracing acceleration structures from the generated geometry
	BuildAccelerationStructures();

	//Create constant buffers for the geometry and the scene
	CreateConstantBuffers();

	//Build shader tables, which define shaders and their local root arguments
	for (UINT i = 0; i < shaderCount; i++)
	{
		BuildShaderTables(i);
	}

	//Create an output 2D texture to store the raytracing result to
	CreateRaytracingOutputResource();
}


void DXRaytracingFallback::CreateAuxilaryDeviceResources()
{
	auto device = dxContext.GetDevice();
	auto commandQueue = dxContext.GetCommandQueue();

	ASRebuildTimer.RestoreDevice(device, commandQueue);
}

//Create raytracing device and commandlist
void DXRaytracingFallback::CreateRaytracingInterfaces()
{
	ID3D12Device* device = dxContext.GetDevice();
	ID3D12GraphicsCommandList* commandList = dxContext.GetCommandList();

	//Let the fallback layer decide on the compute or DXR path
	CreateRaytracingFallbackDeviceFlags createDeviceFlags = CreateRaytracingFallbackDeviceFlags::None;
	ThrowIfFailed(D3D12CreateRaytracingFallbackDevice(device, createDeviceFlags, 0, IID_PPV_ARGS(&fallbackDevice)));
	fallbackDevice->QueryRaytracingCommandList(commandList, IID_PPV_ARGS(&fallbackCommandList));
}

void DXRaytracingFallback::CreateRootSignatures()
{
	//Global Root Signature
	//This is a root signature that is shared across all raytracing shaders invoked during a DispatchRays() call
	{
		CD3DX12_DESCRIPTOR_RANGE ranges[3]; // Perfomance TIP: Order from most frequent to least frequent.
		ranges[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_UAV, 1, 0);  // 1 output texture
		ranges[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 2, 1);  // 2 static index and vertex buffers.
		ranges[2].Init(D3D12_DESCRIPTOR_RANGE_TYPE_UAV, 3, 1);  // 2 bounce map buffers and 1 AO buffers

		CD3DX12_ROOT_PARAMETER rootParameters[GlobalRootSignatureParams::Count];
		rootParameters[GlobalRootSignatureParams::OutputViewSlot].InitAsDescriptorTable(1, &ranges[0]);
		rootParameters[GlobalRootSignatureParams::AccelerationStructureSlot].InitAsShaderResourceView(0);
		rootParameters[GlobalRootSignatureParams::SceneConstantSlot].InitAsConstantBufferView(0);
		rootParameters[GlobalRootSignatureParams::VertexBuffersSlot].InitAsDescriptorTable(1, &ranges[1]);
		rootParameters[GlobalRootSignatureParams::BounceMapSlot].InitAsDescriptorTable(1, &ranges[2]);
		CD3DX12_ROOT_SIGNATURE_DESC globalRootSignatureDesc(ARRAYSIZE(rootParameters), rootParameters);
		SerializeAndCreateRaytracingRootSignature(globalRootSignatureDesc, &raytracingGlobalRootSignature);
	}

	//Local Root Signature
	{
		CD3DX12_ROOT_PARAMETER rootParameters[LocalRootSignatureParams::Count];
		rootParameters[LocalRootSignatureParams::ModelConstantSlot].InitAsConstants(SizeOfInUint32(modelCB), 1);
		CD3DX12_ROOT_SIGNATURE_DESC localRootSignatureDesc(ARRAYSIZE(rootParameters), rootParameters);
		localRootSignatureDesc.Flags = D3D12_ROOT_SIGNATURE_FLAG_LOCAL_ROOT_SIGNATURE;
		SerializeAndCreateRaytracingRootSignature(localRootSignatureDesc, &raytracingLocalRootSignature);
	}

#if USE_NON_NULL_LOCAL_ROOT_SIG 
	// Empty local root signature
	{
		CD3DX12_ROOT_SIGNATURE_DESC localRootSignatureDesc(D3D12_DEFAULT);
		localRootSignatureDesc.Flags = D3D12_ROOT_SIGNATURE_FLAG_LOCAL_ROOT_SIGNATURE;
		SerializeAndCreateRaytracingRootSignature(localRootSignatureDesc, &raytracingLocalRootSignatureEmpty);
	}
#endif
}

void DXRaytracingFallback::SerializeAndCreateRaytracingRootSignature(D3D12_ROOT_SIGNATURE_DESC & desc, Microsoft::WRL::ComPtr<ID3D12RootSignature>* rootSig)
{
	ID3D12Device* device = dxContext.GetDevice();
	ComPtr<ID3DBlob> blob;
	ComPtr<ID3DBlob> error;

	//Init with fallback layer
	ThrowIfFailed(fallbackDevice->D3D12SerializeRootSignature(&desc, D3D_ROOT_SIGNATURE_VERSION_1, &blob, &error));
	ThrowIfFailed(fallbackDevice->CreateRootSignature(1, blob->GetBufferPointer(), blob->GetBufferSize(), IID_PPV_ARGS(&(*rootSig))));

}

//Local root signature and shader association
//This is a root signature that enables a shader to have unique arguments that come from shader tables.
void DXRaytracingFallback::CreateLocalRootSignatureSubobjects(CD3D12_STATE_OBJECT_DESC* raytracingPipeline)
{
	//Local root signature to be used in a ray gen shader

	auto localRootSignature = raytracingPipeline->CreateSubobject<CD3D12_LOCAL_ROOT_SIGNATURE_SUBOBJECT>();
	localRootSignature->SetRootSignature(raytracingLocalRootSignature.Get());
	{
		//Shader association
		auto rootSignatureAssociation = raytracingPipeline->CreateSubobject<CD3D12_SUBOBJECT_TO_EXPORTS_ASSOCIATION_SUBOBJECT>();
		rootSignatureAssociation->SetSubobjectToAssociate(*localRootSignature);
		rootSignatureAssociation->AddExport(hitGroupName);
	}
#if USE_NON_NULL_LOCAL_ROOT_SIG
	// Empty local root signature to be used in a ray gen and a miss shader.
	{
		auto localRootSignature = raytracingPipeline->CreateSubobject<CD3D12_LOCAL_ROOT_SIGNATURE_SUBOBJECT>();
		localRootSignature->SetRootSignature(raytracingLocalRootSignatureEmpty.Get());
		// Shader association
		auto rootSignatureAssociation = raytracingPipeline->CreateSubobject<CD3D12_SUBOBJECT_TO_EXPORTS_ASSOCIATION_SUBOBJECT>();
		rootSignatureAssociation->SetSubobjectToAssociate(*localRootSignature);
		rootSignatureAssociation->AddExport(raygenShaderName);
		rootSignatureAssociation->AddExport(missShaderName);
	}
#endif
}

// Create a raytracing pipeline state object (RTPSO).
// An RTPSO represents a full set of shaders reachable by a DispatchRays() call,
// with all configuration options resolved, such as local signatures and other state
ComPtr<ID3D12RaytracingFallbackStateObject> DXRaytracingFallback::CreateRaytracingPipelineStateObject(CD3DX12_SHADER_BYTECODE shaderCode, UINT shaderIndex)
{
	//Create the 7 subobjects that combine into a RTPSO:
	//Subobjects need to be associated with DXIL exprots (1.e. shaders) either by way of default or explicit associations.
	//Default association applies to every exported shader entrypoint that doesn't have any of the same type of subobject associated with it.
	//The simple example utilizes default shader association except for local root signature subobject
	//which has an explicit association specified purely for demonstation perposes.
	// 1 - DXIL library
	// 1 - Triangle hit group
	// 1 - Shader config
	// 2 - Local root signature and association (for each hit group type, 1 for now)
	// 1 - Global root signature
	// 1 - Pipeline config

	//TODO:Check other examples
	//TODO:NV Workaround? Nonroot stuff?
	CD3D12_STATE_OBJECT_DESC raytracingPipeline{ D3D12_STATE_OBJECT_TYPE_RAYTRACING_PIPELINE };

	//DXIL library
	//This contains the shaders and their entrypoints for the state object.
	//Since shaders are not considered subobjects, they need to be passed in via DXIL library subobjects.
	auto lib = raytracingPipeline.CreateSubobject<CD3D12_DXIL_LIBRARY_SUBOBJECT>();
	lib->SetDXILLibrary(&shaderCode);

	//Define which shader exports to surface from the library.
	//If no shader exports are defined for a DXIL library subobject, all shaders will be surfaced.
	//TODO: Atm everything is exported so could be omitted, note extra stuff.
	{
		lib->DefineExport(raygenShaderName);
		lib->DefineExport(closestHitShaderName);
		lib->DefineExport(missShaderName);
	}

	//Triangle hit group
	//A hit group specifies closest hit, any hit, and intersection shaders to be executed when a ray intersects the geometry's triangle/AABB.
	//TODO: Only triangle geometry with a closest hit shader used atm. (Others not set)
	auto hitGroup = raytracingPipeline.CreateSubobject<CD3D12_HIT_GROUP_SUBOBJECT>();
	hitGroup->SetClosestHitShaderImport(closestHitShaderName);
	hitGroup->SetHitGroupExport(hitGroupName);

	//Shader Config
	//Defines the maximum sizes in bytes for the ray payload and attribute structure.
	auto shaderConfig = raytracingPipeline.CreateSubobject<CD3D12_RAYTRACING_SHADER_CONFIG_SUBOBJECT>();
	UINT payloadSize = sizeof(XMFLOAT4); //float4 colour
	UINT attributeSize = sizeof(XMFLOAT2); //float2 barycentrics
	shaderConfig->Config(payloadSize, attributeSize);

	//Local root signature and shader association
	//This is a root signature that enables a shader to have unique arguments that come from shader tables.
	CreateLocalRootSignatureSubobjects(&raytracingPipeline);


	//Global root signature
	//This is a root signature that is shared across all raytracing shaders invoked during a DispatchRays(); call.
	auto globalRootSignature = raytracingPipeline.CreateSubobject<CD3D12_ROOT_SIGNATURE_SUBOBJECT>();
	globalRootSignature->SetRootSignature(raytracingGlobalRootSignature.Get());

	// Pipeline config
	// Defines the maximum TraceRay() recursion depth.
	auto pipelineConfig = raytracingPipeline.CreateSubobject<CD3D12_RAYTRACING_PIPELINE_CONFIG_SUBOBJECT>();
	//PERFORMACE TIP: Set max recursion depth as low as needed as drivers may apply optimization strategies for low recursion depths.
	pipelineConfig->Config(maxRecursionDepthPerShader[shaderIndex]);

#if _DEBUG
	PrintStateObjectDesc(raytracingPipeline);
#endif

	//Create the state object.
	//(Fallback layer)
	ComPtr<ID3D12RaytracingFallbackStateObject> newFallbackStateObject;
	ThrowIfFailed(fallbackDevice->CreateStateObject(raytracingPipeline, IID_PPV_ARGS(&newFallbackStateObject)), L"Couldn't create DirectX Raytracing state object.\n");

	return newFallbackStateObject;
}

void DXRaytracingFallback::CreateDescriptorHeap()
{
	auto device = dxContext.GetDevice();

	D3D12_DESCRIPTOR_HEAP_DESC descriptorHeapDesc = {};
	//Allocate a heap for 8 descriptors:
	//2 for the bottom and top level acceleration structure fallback wrapper pointers
	//1 for the raytracing output texture Shader Resource View (SRV)
	//2 for the vertex and index buffer SRVs
	//2 for the vertex and normal bounce UAV buffer
	//1 for the AO Test buffer
	descriptorHeapDesc.NumDescriptors = 8;
	descriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV; //Constant buffer, Shader resource, and unordered-access views
	descriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	descriptorHeapDesc.NodeMask = 0;
	device->CreateDescriptorHeap(&descriptorHeapDesc, IID_PPV_ARGS(&descriptorHeap));
	NAME_D3D12_OBJECT(descriptorHeap);

	descriptorSize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
}

void DXRaytracingFallback::BuildGeometry()
{
	auto device = dxContext.GetDevice();

	//Allocate the model data to the upload buffers
	AllocateUploadBuffer(device, sceneModel->indices.data(), sceneModel->indices.size() * sizeof(Index), &indexBuffer.resource);
	AllocateUploadBuffer(device, sceneModel->vertices.data(), sceneModel->vertices.size() * sizeof(Vertex), &vertexBuffer.resource);

	//Vertex buffer is passed to the shader along with index buffer as a descriptor table
	//Vertex buffer descriptor must follow index buffer descriptor in the descriptor heap
	UINT descriptorIndexIB = CreateBufferSRV(&indexBuffer, sceneModel->indices.size(), 0);
	UINT descriptorIndexVB = CreateBufferSRV(&vertexBuffer, sceneModel->vertices.size(), sizeof(Vertex));

	ThrowIfFalse(descriptorIndexVB == descriptorIndexIB + 1, L"Vertex Buffer descriptor index must follow that of Index Buffer descriptor index!");
}

void DXRaytracingFallback::BuildAccelerationStructures()
{
	auto device = dxContext.GetDevice();
	auto commandList = dxContext.GetCommandList();
	auto commandQueue = dxContext.GetCommandQueue();
	auto commandAllocator = dxContext.GetCommandAllocator();

	//Reset the command list for the acceleration structure construction.
	commandList->Reset(commandAllocator, nullptr);

	D3D12_RAYTRACING_GEOMETRY_DESC geometryDesc = {};
	geometryDesc.Type = D3D12_RAYTRACING_GEOMETRY_TYPE_TRIANGLES;
	geometryDesc.Triangles.IndexBuffer = indexBuffer.resource->GetGPUVirtualAddress();
	geometryDesc.Triangles.IndexCount = static_cast<UINT>(indexBuffer.resource->GetDesc().Width) / sizeof(Index);
	geometryDesc.Triangles.IndexFormat = DXGI_FORMAT_R32_UINT; //UINT 32 bits
	geometryDesc.Triangles.Transform = 0;
	geometryDesc.Triangles.VertexFormat = DXGI_FORMAT_R32G32B32_FLOAT;
	geometryDesc.Triangles.VertexCount = static_cast<UINT>(vertexBuffer.resource->GetDesc().Width) / sizeof(Vertex);
	geometryDesc.Triangles.VertexBuffer.StartAddress = vertexBuffer.resource->GetGPUVirtualAddress();
	geometryDesc.Triangles.VertexBuffer.StrideInBytes = sizeof(Vertex); //UINT32

	//Mark the geometry as opaque, do this whenever applicable as it can enable important ray processing optimizations.
	//Note: when rays encounter opaque geometry an any hit shader will not be executed whether it is present or not.
	geometryDesc.Flags = D3D12_RAYTRACING_GEOMETRY_FLAG_OPAQUE;

	//Save the geometry desc for update calls
	blGeometryDesc = geometryDesc;

	ASBuildFlags = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_NONE;

	{
		switch (asBuildType)
		{
		case FastBuild:
			ASBuildFlags = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_BUILD;
			break;
		case FastTrace:
			ASBuildFlags = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_TRACE;
			break;
		default:
			break;
		}

		if (ASAllowUpdate)
		{
			ASBuildFlags |= D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_ALLOW_UPDATE;
		}
	}

	//Get required sizes for an acceleration structure.
	//D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAGS buildFlags = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PREFER_FAST_TRACE;
	D3D12_GET_RAYTRACING_ACCELERATION_STRUCTURE_PREBUILD_INFO_DESC prebuildInfoDesc = {};
	prebuildInfoDesc.DescsLayout = D3D12_ELEMENTS_LAYOUT_ARRAY;
	prebuildInfoDesc.Flags = ASBuildFlags;
	prebuildInfoDesc.NumDescs = 1;

	prebuildInfoDesc.Type = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL;
	prebuildInfoDesc.pGeometryDescs = nullptr;

	fallbackDevice->GetRaytracingAccelerationStructurePrebuildInfo(&prebuildInfoDesc, &topLevelPrebuildInfo);

	ThrowIfFalse(topLevelPrebuildInfo.ResultDataMaxSizeInBytes > 0);

	prebuildInfoDesc.Type = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL;
	prebuildInfoDesc.pGeometryDescs = &geometryDesc;

	fallbackDevice->GetRaytracingAccelerationStructurePrebuildInfo(&prebuildInfoDesc, &bottomLevelPrebuildInfo);

	ThrowIfFalse(bottomLevelPrebuildInfo.ResultDataMaxSizeInBytes > 0);

	AllocateUAVBuffer(device, max(topLevelPrebuildInfo.ScratchDataSizeInBytes, bottomLevelPrebuildInfo.ScratchDataSizeInBytes), &accelerationScratchResource, D3D12_RESOURCE_STATE_UNORDERED_ACCESS, L"ScratchResource");

	// Allocate resources for acceleration structures.
	// Acceleration structures can only be placed in resources that are created in the default heap (or custom heap equivalent). 
	// Default heap is OK since the application doesn�t need CPU read/write access to them. 
	// The resources that will contain acceleration structures must be created in the state D3D12_RESOURCE_STATE_RAYTRACING_ACCELERATION_STRUCTURE, 
	// and must have resource flag D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS. The ALLOW_UNORDERED_ACCESS requirement simply acknowledges both: 
	//  - the system will be doing this type of access in its implementation of acceleration structure builds behind the scenes.
	//  - from the app point of view, synchronization of writes/reads to acceleration structures is accomplished using UAV barriers.

	{
		D3D12_RESOURCE_STATES initialResourceState = fallbackDevice->GetAccelerationStructureResourceState();

		AllocateUAVBuffer(device, bottomLevelPrebuildInfo.ResultDataMaxSizeInBytes, &bottomLevelAccelerationStructure, initialResourceState, L"BottomLevelAccelerationStructure");
		AllocateUAVBuffer(device, topLevelPrebuildInfo.ResultDataMaxSizeInBytes, &topLevelAccelerationStructure, initialResourceState, L"TopLevelAccelerationStructure");

	}

	// Note on Emulated GPU pointers (AKA Wrapped pointers) requirement in Fallback Layer:
	// The primary point of divergence between the DXR API and the compute-based Fallback layer is the handling of GPU pointers. 
	// DXR fundamentally requires that GPUs be able to dynamically read from arbitrary addresses in GPU memory. 
	// The existing Direct Compute API today is more rigid than DXR and requires apps to explicitly inform the GPU what blocks of memory it will access with SRVs/UAVs.
	// In order to handle the requirements of DXR, the Fallback Layer uses the concept of Emulated GPU pointers, 
	// which requires apps to create views around all memory they will access for raytracing, 
	// but retains the DXR-like flexibility of only needing to bind the top level acceleration structure at DispatchRays.
	//
	// The Fallback Layer interface uses WRAPPED_GPU_POINTER to encapsulate the underlying pointer
	// which will either be an emulated GPU pointer for the compute - based path or a GPU_VIRTUAL_ADDRESS for the DXR path.

	// Create an instance desc for the bottom-level acceleration structure.
	//ComPtr<ID3D12Resource> instanceDescs;

	//Fallback layer acceleration structure instance
	D3D12_RAYTRACING_FALLBACK_INSTANCE_DESC instanceDesc = {};
	instanceDesc.Transform[0] = instanceDesc.Transform[5] = instanceDesc.Transform[10] = 1; //Identity matrix
	instanceDesc.InstanceMask = 1;
	UINT numBufferElements = static_cast<UINT>(bottomLevelPrebuildInfo.ResultDataMaxSizeInBytes) / sizeof(UINT32);
	instanceDesc.AccelerationStructure = CreateFallbackWrappedPointer(bottomLevelAccelerationStructure.Get(), numBufferElements, &bottomLevelASDescriptorHeapIndex);
	AllocateUploadBuffer(device, &instanceDesc, sizeof(instanceDesc), &instanceDescs, L"InstanceDescs");

	//Create a wrapped pointer to the acceleration structure.
	numBufferElements = static_cast<UINT>(topLevelPrebuildInfo.ResultDataMaxSizeInBytes) / sizeof(UINT32);
	fallbackTopLevelAccelerationStructurePointer = CreateFallbackWrappedPointer(topLevelAccelerationStructure.Get(), numBufferElements, &topLevelASDescriptorHeapIndex);

	//Bottom level Acceleration Structure Description
	D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC bottomLevelBuildDesc = {};
	{
		bottomLevelBuildDesc.DescsLayout = D3D12_ELEMENTS_LAYOUT_ARRAY;
		bottomLevelBuildDesc.Flags = ASBuildFlags;
		bottomLevelBuildDesc.ScratchAccelerationStructureData = { accelerationScratchResource->GetGPUVirtualAddress(), accelerationScratchResource->GetDesc().Width };
		bottomLevelBuildDesc.Type = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL;
		bottomLevelBuildDesc.DestAccelerationStructureData = { bottomLevelAccelerationStructure->GetGPUVirtualAddress(), bottomLevelPrebuildInfo.ResultDataMaxSizeInBytes };
		bottomLevelBuildDesc.NumDescs = 1; //TODO #elements?
		bottomLevelBuildDesc.pGeometryDescs = &geometryDesc;
	}

	//Top level Acceleration Structure Description
	D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC topLevelBuildDesc = bottomLevelBuildDesc;
	{
		topLevelBuildDesc.Type = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL;
		topLevelBuildDesc.DestAccelerationStructureData = { topLevelAccelerationStructure->GetGPUVirtualAddress(), topLevelPrebuildInfo.ResultDataMaxSizeInBytes };
		topLevelBuildDesc.NumDescs = 1;
		topLevelBuildDesc.pGeometryDescs = nullptr;
		topLevelBuildDesc.InstanceDescs = instanceDescs->GetGPUVirtualAddress();
		topLevelBuildDesc.ScratchAccelerationStructureData = { accelerationScratchResource->GetGPUVirtualAddress(), accelerationScratchResource->GetDesc().Width };
	}

	//Build acceleration structure using the fallback layer

	//Set the descriptor heaps to be used during acceleration structure build for the fallback layer.
	ID3D12DescriptorHeap *pDescriptorHeaps[] = { descriptorHeap.Get() };
	fallbackCommandList->SetDescriptorHeaps(ARRAYSIZE(pDescriptorHeaps), pDescriptorHeaps);


	fallbackCommandList->BuildRaytracingAccelerationStructure(&bottomLevelBuildDesc);
	commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::UAV(bottomLevelAccelerationStructure.Get()));
	fallbackCommandList->BuildRaytracingAccelerationStructure(&topLevelBuildDesc);


	//Start acceleration structure construction.
	dxContext.ExecuteCommandList();

	//Wait for GPU to finish as the locally created temporary GPU resource will get released once we go out of scope.
	dxContext.WaitForGpu();
}

void DXRaytracingFallback::UpdateAccelerationStructures(bool forceRebuild, bool update)
{
	auto commandList = dxContext.GetCommandList();


	if (forceRebuild)
	{
		//Update bottom level AS
		{
			ThrowIfFalse(bottomLevelPrebuildInfo.ScratchDataSizeInBytes <= accelerationScratchResource->GetDesc().Width, L"Insufficient scratch buffer size provided!");

			D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC bottomLevelBuildDesc = {};
			{
				bottomLevelBuildDesc.Type = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL;
				bottomLevelBuildDesc.DescsLayout = D3D12_ELEMENTS_LAYOUT_ARRAY;
				bottomLevelBuildDesc.Flags = ASBuildFlags;
				if (update)
				{
					//Not supported, flag is ignored.
					bottomLevelBuildDesc.Flags |= D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PERFORM_UPDATE;
				}
				bottomLevelBuildDesc.ScratchAccelerationStructureData = { accelerationScratchResource->GetGPUVirtualAddress(), accelerationScratchResource->GetDesc().Width };
				bottomLevelBuildDesc.DestAccelerationStructureData = { bottomLevelAccelerationStructure->GetGPUVirtualAddress(), bottomLevelPrebuildInfo.ResultDataMaxSizeInBytes };
				bottomLevelBuildDesc.NumDescs = 1;
				bottomLevelBuildDesc.pGeometryDescs = &blGeometryDesc;
			}

			// Set the descriptor heaps to be used during acceleration structure build for the Fallback Layer.
			ID3D12DescriptorHeap *pDescriptorHeaps[] = { descriptorHeap.Get() };
			fallbackCommandList.Get()->SetDescriptorHeaps(ARRAYSIZE(pDescriptorHeaps), pDescriptorHeaps);
			fallbackCommandList.Get()->BuildRaytracingAccelerationStructure(&bottomLevelBuildDesc);

			commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::UAV(bottomLevelAccelerationStructure.Get()));
		}
		//Update top level AS
		{
			D3D12_BUILD_RAYTRACING_ACCELERATION_STRUCTURE_DESC topLevelBuildDesc = {};
			{
				topLevelBuildDesc.Type = D3D12_RAYTRACING_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL;
				topLevelBuildDesc.DescsLayout = D3D12_ELEMENTS_LAYOUT_ARRAY;
				topLevelBuildDesc.Flags = ASBuildFlags;
				if (update)
				{
					topLevelBuildDesc.Flags |= D3D12_RAYTRACING_ACCELERATION_STRUCTURE_BUILD_FLAG_PERFORM_UPDATE;
				}
				topLevelBuildDesc.ScratchAccelerationStructureData = { accelerationScratchResource->GetGPUVirtualAddress(), accelerationScratchResource->GetDesc().Width };
				topLevelBuildDesc.DestAccelerationStructureData = { topLevelAccelerationStructure->GetGPUVirtualAddress(), topLevelPrebuildInfo.ResultDataMaxSizeInBytes };
				topLevelBuildDesc.NumDescs = 1;
			}

			topLevelBuildDesc.InstanceDescs = instanceDescs.Get()->GetGPUVirtualAddress();
			// Set the descriptor heaps to be used during acceleration structure build for the Fallback Layer.
			ID3D12DescriptorHeap *pDescriptorHeaps[] = { descriptorHeap.Get() };
			fallbackCommandList.Get()->SetDescriptorHeaps(ARRAYSIZE(pDescriptorHeaps), pDescriptorHeaps);
			fallbackCommandList.Get()->BuildRaytracingAccelerationStructure(&topLevelBuildDesc);

			commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::UAV(topLevelAccelerationStructure.Get()));
		}
	}
}

//Create constant buffers
void DXRaytracingFallback::CreateConstantBuffers()
{
	auto device = dxContext.GetDevice();
	auto frameCount = dxContext.GetBackBufferCount();

	//Create the constant buffer memory and map the CPU and GPU addresses
	const D3D12_HEAP_PROPERTIES uploadHeapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);

	//Allocate one constant buffer per frame, since it gets updated every frame
	size_t cbSize = frameCount * sizeof(AlignedSceneConstantBuffer);
	const D3D12_RESOURCE_DESC constantBufferDesc = CD3DX12_RESOURCE_DESC::Buffer(cbSize);

	ThrowIfFailed(device->CreateCommittedResource
	(
		&uploadHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&constantBufferDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&perFrameConstants)
	));

	//Map the constant buffer and cache its heap pointers.
	//We don't unmap this until the program closes. Keeping buffer mapped for the lifetime of the resource is OK.
	CD3DX12_RANGE readRange(0, 0); //We do not intend to read from this resource on the CPU
	ThrowIfFailed(perFrameConstants->Map(0, nullptr, reinterpret_cast<void**>(&mappedConstantData)));
}

//Build shader tables
//This encapsulates all shaders and the arguments for their local root signatures.
void DXRaytracingFallback::BuildShaderTables(UINT shaderIndex)
{
	auto device = dxContext.GetDevice();

	void* rayGenShaderIdentifier;
	void* missShaderIdentifier;
	void* hitGroupShaderIdentifier;


	//Get shader identifiers, using the fallback layer

	rayGenShaderIdentifier = fallbackStateObject[shaderIndex]->GetShaderIdentifier(raygenShaderName);
	missShaderIdentifier = fallbackStateObject[shaderIndex]->GetShaderIdentifier(missShaderName);
	hitGroupShaderIdentifier = fallbackStateObject[shaderIndex]->GetShaderIdentifier(hitGroupName);

	UINT shaderIdentifierSize = fallbackDevice->GetShaderIdentifierSize();

	//Ray gen shader table
	{
		UINT numShaderRecords = 1;
		UINT shaderRecordSize = shaderIdentifierSize;
		ShaderTable rayGenShaderTable(device, numShaderRecords, shaderRecordSize, L"RayGenShaderTable");
		rayGenShaderTable.push_back(ShaderRecord(rayGenShaderIdentifier, shaderIdentifierSize));
		rayGenShaderTableResource[shaderIndex] = rayGenShaderTable.GetResource();
	}

	//Miss shader table
	{
		UINT numShaderRecords = 1;
		UINT shaderRecordSize = shaderIdentifierSize;
		ShaderTable missShaderTable(device, numShaderRecords, shaderRecordSize, L"MissShaderTable");
		missShaderTable.push_back(ShaderRecord(missShaderIdentifier, shaderIdentifierSize));
		missShaderTableResource[shaderIndex] = missShaderTable.GetResource();
	}

	//Hit group shader table
	{
		struct RootArguments
		{
			ModelConstantBuffer cb;
		} rootArguments;
		rootArguments.cb = modelCB;

		UINT numShaderRecords = 1;
		UINT shaderRecordSize = shaderIdentifierSize + sizeof(rootArguments);
		ShaderTable hitGroupShaderTable(device, numShaderRecords, shaderRecordSize, L"HitGroupShaderTable");
		hitGroupShaderTable.push_back(ShaderRecord(hitGroupShaderIdentifier, shaderIdentifierSize, &rootArguments, sizeof(RootArguments)));
		hitGroupShaderTableResource[shaderIndex] = hitGroupShaderTable.GetResource();
	}
}

//Create 2D output texture for raytracing
void DXRaytracingFallback::CreateRaytracingOutputResource()
{
	auto device = dxContext.GetDevice();
	auto backbufferFormat = dxContext.GetBackBufferFormat();

	//Create the output resource, the dimensions and format should match the swapchain
	auto uavDesc = CD3DX12_RESOURCE_DESC::Tex2D(backbufferFormat, dxContext.GetWidth(), dxContext.GetHeight(), 1, 1, 1, 0, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);

	auto defaultHeapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
	ThrowIfFailed(device->CreateCommittedResource(&defaultHeapProperties, D3D12_HEAP_FLAG_NONE, &uavDesc, D3D12_RESOURCE_STATE_UNORDERED_ACCESS, nullptr, IID_PPV_ARGS(&raytracingOutput)));
	NAME_D3D12_OBJECT(raytracingOutput);

	D3D12_CPU_DESCRIPTOR_HANDLE uavDescriptorHandle;
	raytracingOutputResourceUAVDescriptorHeapIndex = AllocateDescriptor(&uavDescriptorHandle, raytracingOutputResourceUAVDescriptorHeapIndex);
	D3D12_UNORDERED_ACCESS_VIEW_DESC UAVDesc = {};
	UAVDesc.ViewDimension = D3D12_UAV_DIMENSION_TEXTURE2D;
	device->CreateUnorderedAccessView(raytracingOutput.Get(), nullptr, &UAVDesc, uavDescriptorHandle);
	raytracingOutputResourceUAVGpuDescriptor = CD3DX12_GPU_DESCRIPTOR_HANDLE(descriptorHeap->GetGPUDescriptorHandleForHeapStart(), raytracingOutputResourceUAVDescriptorHeapIndex, descriptorSize);
}

void DXRaytracingFallback::CreateWindowSizeDependentResourcesForRT()
{
	CreateRaytracingOutputResource();

	//For simplicity we will rebuild the shader tables
	for (UINT i = 0; i < shaderCount; i++)
	{
		BuildShaderTables(i);
	}
}

void DXRaytracingFallback::UpdateForSizeChange(UINT newWidth, UINT newHeight)
{
	dxContext.UpdateForSizeChange(newWidth, newHeight);
}

//Create a wrapped pointer for the Fallback layer
WRAPPED_GPU_POINTER DXRaytracingFallback::CreateFallbackWrappedPointer(ID3D12Resource * resource, UINT bufferNumElements, UINT* descriptorHeapIndex)
{
	auto device = dxContext.GetDevice();

	D3D12_UNORDERED_ACCESS_VIEW_DESC rawBufferUavDesc = {};
	rawBufferUavDesc.ViewDimension = D3D12_UAV_DIMENSION_BUFFER;
	rawBufferUavDesc.Buffer.Flags = D3D12_BUFFER_UAV_FLAG_RAW;
	rawBufferUavDesc.Format = DXGI_FORMAT_R32_TYPELESS;
	rawBufferUavDesc.Buffer.NumElements = bufferNumElements;

	D3D12_CPU_DESCRIPTOR_HANDLE bottomLevelDescriptor;

	UINT newDescriptorHeapIndex = 0;
	if (!fallbackDevice->UsingRaytracingDriver())
	{
		//Compute fallback needs a valid descriptor index when creating a wrapped pointer.
		newDescriptorHeapIndex = AllocateDescriptor(&bottomLevelDescriptor);
		device->CreateUnorderedAccessView(resource, nullptr, &rawBufferUavDesc, bottomLevelDescriptor);

		*descriptorHeapIndex = newDescriptorHeapIndex;
	}

	return fallbackDevice->GetWrappedPointerSimple(newDescriptorHeapIndex, resource->GetGPUVirtualAddress());
}

//Allocate a descriptor and return its index.
//If the passed descriptorIndexToUse is valid, it will be used instead of allocating a new one.
UINT DXRaytracingFallback::AllocateDescriptor(D3D12_CPU_DESCRIPTOR_HANDLE * cpuDescriptor, UINT descriptorIndexToUse)
{
	auto descriptorHeapCpuBase = descriptorHeap->GetCPUDescriptorHandleForHeapStart();
	if (descriptorIndexToUse >= descriptorHeap->GetDesc().NumDescriptors)
	{
		ThrowIfFalse(descriptorsAllocated < descriptorHeap->GetDesc().NumDescriptors, L"Ran out of descriptors on the heap!");
		descriptorIndexToUse = descriptorsAllocated++;
	}
	*cpuDescriptor = CD3DX12_CPU_DESCRIPTOR_HANDLE(descriptorHeapCpuBase, descriptorIndexToUse, descriptorSize);
	return descriptorIndexToUse;
}

//Create Shader Resource View for a buffer
UINT DXRaytracingFallback::CreateBufferSRV(D3DBuffer* buffer, UINT numElements, UINT elementSize)
{
	auto device = dxContext.GetDevice();

	//SRV
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Buffer.NumElements = numElements;
	if (elementSize == 0)
	{
		srvDesc.Format = DXGI_FORMAT_R32_TYPELESS;
		srvDesc.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_RAW;
		srvDesc.Buffer.StructureByteStride = 0;
	}
	else
	{
		srvDesc.Format = DXGI_FORMAT_UNKNOWN;
		srvDesc.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_NONE;
		srvDesc.Buffer.StructureByteStride = elementSize;
	}
	UINT descriptorIndex = AllocateDescriptor(&buffer->cpuDescriptorHandle);
	device->CreateShaderResourceView(buffer->resource.Get(), &srvDesc, buffer->cpuDescriptorHandle);
	buffer->gpuDescriptorHandle = CD3DX12_GPU_DESCRIPTOR_HANDLE(descriptorHeap->GetGPUDescriptorHandleForHeapStart(), descriptorIndex, descriptorSize);
	return descriptorIndex;
}

void DXRaytracingFallback::ReleaseWindowSizeDependentResourcesForRT()
{
	for (size_t i = 0; i < shaderCount; i++)
	{
		rayGenShaderTableResource[i].Reset();
		missShaderTableResource[i].Reset();
		hitGroupShaderTableResource[i].Reset();
	}

	raytracingOutput.Reset();
}

void DXRaytracingFallback::ReleaseDeviceDependentResourcesForRT()
{
	ASRebuildTimer.ReleaseDevice();

	fallbackDevice.Reset();
	fallbackCommandList.Reset();

	for (auto stateObject : fallbackStateObject)
	{
		stateObject.Reset();
	}

	raytracingGlobalRootSignature.Reset();
	raytracingLocalRootSignature.Reset();
#if USE_NON_NULL_LOCAL_ROOT_SIG 
	raytracingLocalRootSignatureEmpty.Reset();
#endif
	descriptorHeap.Reset();
	descriptorsAllocated = 0;
	raytracingOutputResourceUAVDescriptorHeapIndex = UINT_MAX;
	indexBuffer.resource.Reset();
	vertexBuffer.resource.Reset();

	perFrameConstants.Reset();

	for (size_t i = 0; i < shaderCount; i++)
	{
		rayGenShaderTableResource[i].Reset();
		missShaderTableResource[i].Reset();
		hitGroupShaderTableResource[i].Reset();
	}

	bottomLevelAccelerationStructure.Reset();
	topLevelAccelerationStructure.Reset();
}

void DXRaytracingFallback::OnDeviceLost()
{
	ReleaseWindowSizeDependentResourcesForRT();
	ReleaseDeviceDependentResourcesForRT();
}

void DXRaytracingFallback::OnDeviceRestored()
{
	CreateDeviceDependentResourcesForRT();
	CreateWindowSizeDependentResourcesForRT();
}


//Test stuff
void DXRaytracingFallback::InitializeBumpMap()
{
	auto device = dxContext.GetDevice();

	UINT numElements = dxContext.GetWidth() * dxContext.GetHeight();

	//Create bounce vertex and normal resources
	D3D12_RESOURCE_DESC uavDesc = CD3DX12_RESOURCE_DESC::Buffer(numElements * sizeof(XMFLOAT4), D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);

	auto defaultHeapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
	ThrowIfFailed(device->CreateCommittedResource(&defaultHeapProperties, D3D12_HEAP_FLAG_NONE, &uavDesc, D3D12_RESOURCE_STATE_UNORDERED_ACCESS, nullptr, IID_PPV_ARGS(&bounceMapVertexBuffer)));
	bounceMapVertexBuffer->SetName(L"bounceMapVertexBuffer");

	ThrowIfFailed(device->CreateCommittedResource(&defaultHeapProperties, D3D12_HEAP_FLAG_NONE, &uavDesc, D3D12_RESOURCE_STATE_UNORDERED_ACCESS, nullptr, IID_PPV_ARGS(&bounceMapNormalBuffer)));
	bounceMapNormalBuffer->SetName(L"bounceMapNormalBuffer");

	//Create descriptors
	D3D12_UNORDERED_ACCESS_VIEW_DESC UAVDesc = {};
	UAVDesc.ViewDimension = D3D12_UAV_DIMENSION_BUFFER;
	UAVDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	UAVDesc.Buffer.FirstElement = 0;
	UAVDesc.Buffer.NumElements = numElements;
	UAVDesc.Buffer.Flags = D3D12_BUFFER_UAV_FLAG_NONE;

	//Allocate the bounce map vertices HeapIndex, UAV, and GPU Descriptor
	D3D12_CPU_DESCRIPTOR_HANDLE uavDescriptorHandle;
	bounceMapVertexUAVDescriptorHeapIndex = AllocateDescriptor(&uavDescriptorHandle, bounceMapVertexUAVDescriptorHeapIndex);

	device->CreateUnorderedAccessView(bounceMapVertexBuffer.Get(), nullptr, &UAVDesc, uavDescriptorHandle);

	bounceMapVertexUAVGpuDescriptor = CD3DX12_GPU_DESCRIPTOR_HANDLE(descriptorHeap->GetGPUDescriptorHandleForHeapStart(), bounceMapVertexUAVDescriptorHeapIndex, descriptorSize);

	//Allocate the bounce map normal HeapIndex, UAV, and GPU Descriptor
	bounceMapNormalUAVDescriptorHeapIndex = AllocateDescriptor(&uavDescriptorHandle, bounceMapNormalUAVDescriptorHeapIndex);

	device->CreateUnorderedAccessView(bounceMapNormalBuffer.Get(), nullptr, &UAVDesc, uavDescriptorHandle);

	bounceMapNormalUAVGpuDescriptor = CD3DX12_GPU_DESCRIPTOR_HANDLE(descriptorHeap->GetGPUDescriptorHandleForHeapStart(), bounceMapNormalUAVDescriptorHeapIndex, descriptorSize);

	ThrowIfFalse(bounceMapNormalUAVDescriptorHeapIndex == bounceMapVertexUAVDescriptorHeapIndex + 1, L"Normal Buffer descriptor index must follow that of Vertex Buffer descriptor index!");
}

void DXRaytracingFallback::InitializeAO()
{
	auto device = dxContext.GetDevice();

	UINT numElements = dxContext.GetWidth() * dxContext.GetHeight();

	//Create AO array buffer on device
	D3D12_RESOURCE_DESC uavDesc = CD3DX12_RESOURCE_DESC::Buffer(numElements * sizeof(float), D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);

	auto UAHeapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
	ThrowIfFailed(device->CreateCommittedResource(&UAHeapProperties, D3D12_HEAP_FLAG_NONE, &uavDesc, D3D12_RESOURCE_STATE_UNORDERED_ACCESS, nullptr, IID_PPV_ARGS(&AOTestBuffer)));
	AOTestBuffer->SetName(L"AOTestBuffer");

	D3D12_UNORDERED_ACCESS_VIEW_DESC UAVDesc = {};
	UAVDesc.ViewDimension = D3D12_UAV_DIMENSION_BUFFER;
	UAVDesc.Format = DXGI_FORMAT_R32_FLOAT;
	UAVDesc.Buffer.FirstElement = 0;
	UAVDesc.Buffer.NumElements = numElements;
	UAVDesc.Buffer.Flags = D3D12_BUFFER_UAV_FLAG_NONE;

	//Allocate the AOTest HeapIndex, UAV and GPU Descriptor
	D3D12_CPU_DESCRIPTOR_HANDLE uavDescriptorHandle;
	AOTestUAVDescriptorHeapIndex = AllocateDescriptor(&uavDescriptorHandle, AOTestUAVDescriptorHeapIndex);

	device->CreateUnorderedAccessView(AOTestBuffer.Get(), nullptr, &UAVDesc, uavDescriptorHandle);

	AOTestUAVGpuDescriptor = CD3DX12_GPU_DESCRIPTOR_HANDLE(descriptorHeap->GetGPUDescriptorHandleForHeapStart(), AOTestUAVDescriptorHeapIndex, descriptorSize);

	//Create AO array buffer on system
	D3D12_RESOURCE_DESC readVDesc = CD3DX12_RESOURCE_DESC::Buffer(numElements * sizeof(float), D3D12_RESOURCE_FLAG_NONE);

	auto readHeapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_READBACK);
	ThrowIfFailed(device->CreateCommittedResource(&readHeapProperties, D3D12_HEAP_FLAG_NONE, &readVDesc, D3D12_RESOURCE_STATE_COPY_DEST, nullptr, IID_PPV_ARGS(&AOReadbackBuffer)));
	AOReadbackBuffer->SetName(L"AOReadbackBuffer");
}

void DXRaytracingFallback::ReadbackAOData()
{
	auto commandList = dxContext.GetCommandList();

	//Switch state to Copy
	D3D12_RESOURCE_BARRIER preCopyBarrier;
	preCopyBarrier = CD3DX12_RESOURCE_BARRIER::Transition(AOTestBuffer.Get(), D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_COPY_SOURCE);
	commandList->ResourceBarrier(1, &preCopyBarrier);

	commandList->CopyResource(AOReadbackBuffer.Get(), AOTestBuffer.Get());

	D3D12_RESOURCE_BARRIER postCopyBarrier;
	postCopyBarrier = CD3DX12_RESOURCE_BARRIER::Transition(AOTestBuffer.Get(), D3D12_RESOURCE_STATE_COPY_SOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);


}